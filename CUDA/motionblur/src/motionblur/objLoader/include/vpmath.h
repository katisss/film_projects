#ifndef _VPMATH_H
#define _VPMATH_H
#include <math.h>
#include <float.h>
#if WIN32
#include <windows.h>
#include <windef.h>
#endif
#ifndef INFINITY
#define INFINITY 10e10f
#endif
#ifndef RAY_EPSILON
#define RAY_EPSILON 1e-3
#endif

#ifndef M_PI
#define M_PI 3.1415926
#endif

#ifndef max
#define max(a,b)            (((a) > (b)) ? (a) : (b))
#endif

#ifndef min
#define min(a,b)            (((a) < (b)) ? (a) : (b))
#endif

typedef float Float;
class Point;
#define RAD(x) (M_PI*(x)/180.0)
#define DEG(x) (180.0*(x)/M_PI)

// Geometry Classes
class Vector {
public:
	// Vector Constructors
	Vector()
		: x(0.), y(0.), z(0.) {
	}
	Vector(Float xx, Float yy, Float zz)
		: x(xx), y(yy), z(zz) {
	}
	explicit Vector(const Point &p);
	// Vector Methods
	Vector operator+(const Vector &v) const {
		return Vector(x + v.x, y + v.y, z + v.z);
	}
	
	Vector& operator+=(const Vector &v) {
		x += v.x; y += v.y; z += v.z;
		return *this;
	}
	Vector operator-(const Vector &v) const {
		return Vector(x - v.x, y - v.y, z - v.z);
	}
	
	Vector& operator-=(const Vector &v) {
		x -= v.x; y -= v.y; z -= v.z;
		return *this;
	}
	Vector operator*(Float f) const {
		return Vector(f*x, f*y, f*z);
	}
	
	Vector &operator*=(Float f) {
		x *= f; y *= f; z *= f;
		return *this;
	}
	Vector operator/(Float f) const {
		Float inv = 1.f/f;
		return Vector(x * inv, y * inv, z * inv);
	}
	
	Vector &operator/=(Float f) {
		Float inv = 1.f/f;
		x *= inv; y *= inv; z *= inv;
		return *this;
	}
	Vector operator-() const {
		return Vector(-x, -y, -z);
	}
	Float LengthSquared() const { return x*x + y*y + z*z; }
	Float Length() const { return sqrtf( LengthSquared() ); }
	Vector Hat(float epsilon=0.f) const { return (*this)/(Length()+epsilon); }

	// debug
	float & operator[](unsigned int i){return (&x)[i];}
	float const& operator[](unsigned int i)const{return (&x)[i];}

	// Vector Public Data
	Float x, y, z;
};

class Point {
public:
	// Point Constructors
	Point()
		: x(0.), y(0.), z(0.) {
	}
	Point(Float xx, Float yy, Float zz)
		: x(xx), y(yy), z(zz) {
	}
	// Point Methods
	Point operator+(const Vector &v) const {
		return Point(x + v.x, y + v.y, z + v.z);
	}
	
	Point &operator+=(const Vector &v) {
		x += v.x; y += v.y; z += v.z;
		return *this;
	}
	
	Vector operator-(const Point &p) const {
		return Vector(x - p.x, y - p.y, z - p.z);
	}
	
	Point operator-(const Vector &v) const {
		return Point(x - v.x, y - v.y, z - v.z);
	}
	
	Point &operator-=(const Vector &v) {
		x -= v.x; y -= v.y; z -= v.z;
		return *this;
	}
	Point &operator+=(const Point &p) {
		x += p.x; y += p.y; z += p.z;
		return *this;
	}
	Point operator+(const Point &p) {
		return Point(x + p.x, y + p.y, z + p.z);
	}
	Point operator* (Float f) const {
		return Point(f*x, f*y, f*z);
	}
	Point &operator*=(Float f) {
		x *= f; y *= f; z *= f;
		return *this;
	}
	// Point Public Data
	Float x,y,z;

	//debug
	float & operator[](unsigned int i){return (&x)[i];}
	float const& operator[](unsigned int i)const{return (&x)[i];}
};

class Ray {
public:
	// Ray Public Methods
	Ray(): mint((float)RAY_EPSILON), maxt((float)INFINITY), time(0.f) {}
	Ray(const Point &origin, const Vector &direction,
		float start = RAY_EPSILON, float end = INFINITY, float t = 0.f)
		: o(origin), d(direction), mint(start), maxt(end), time(t) { }
	Point operator()(float t) const { return o + d * t; }
	// Ray Public Data
	Point o;
	Vector d;
	mutable float mint, maxt;
	float time;
};

// Geometry Classes
class Quaternion {
public:
	// Vector Constructors
	Quaternion(): w(0.) { }
	Quaternion(Float iw, Vector iv): w(iw) {
		this->v = iv; 
	}
	// Quaternion Public Data
	Float w; Vector v;
};

// Geometry Inline Functions
inline Vector::Vector(const Point &p)
	: x(p.x), y(p.y), z(p.z) {
}
inline Vector operator*(Float f, const Vector &v) { return v*f; }
inline Float Dot(const Vector &v1, const Vector &v2) {
	return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
}
inline Vector Cross(const Vector &v1, const Vector &v2) {
	return Vector((v1.y * v2.z) - (v1.z * v2.y),
                  (v1.z * v2.x) - (v1.x * v2.z),
                  (v1.x * v2.y) - (v1.y * v2.x));
}
inline void CoordinateSystem(const Vector &v1,	Vector *v2, Vector *v3) {
	if (fabsf(v1.x) > fabsf(v1.y)) {
	        Float invLen = 1.f / sqrtf(v1.x*v1.x + v1.z*v1.z);
	        *v2 = Vector(-v1.z * invLen, 0.f, v1.x * invLen);
	}
	else {
		Float invLen = 1.f / sqrtf(v1.y*v1.y + v1.z*v1.z);
		*v2 = Vector(0.f, v1.z * invLen, -v1.y * invLen);
	}
	*v3 = Cross(v1, *v2);
}
inline Float Distance(const Point &p1, const Point &p2) {
	return (p1 - p2).Length();
}
inline Float DistanceSquared(const Point &p1, const Point &p2) {
	return (p1 - p2).LengthSquared();
}
inline Point operator*(Float f, const Point &p) { return p*f; }

inline Vector Direction(Float sintheta, Float costheta, Float phi) {
	return Vector(sintheta * sin(phi), costheta, sintheta * cos(phi));
}
inline Vector SphericalDirection(Float sintheta, Float costheta,
		Float phi, const Vector &x, const Vector &y,
		const Vector &z) {
	return sintheta * cos(phi) * x +
		sintheta * sin(phi) * y + costheta * z;
}
inline Float SphericalTheta(const Vector &v) {
	return acos(v.y);
}
inline Float SphericalPhi(const Vector &v) {
	return atan2(v.z, v.x);
}

class BBox {
public:
	// BBox Public Methods
	BBox() {
		pMin = Point( INFINITY,  INFINITY,  INFINITY);
		pMax = Point(-INFINITY, -INFINITY, -INFINITY);
	}
	BBox(const Point &p) : pMin(p), pMax(p) { }
	BBox(const Point &p1, const Point &p2) {
		pMin = Point(min(p1.x, p2.x),
					 min(p1.y, p2.y),
					 min(p1.z, p2.z));
		pMax = Point(max(p1.x, p2.x),
					 max(p1.y, p2.y),
					 max(p1.z, p2.z));
	}

//	friend BBox Union(const BBox &b, const Point &p);
//	friend BBox Union(const BBox &b, const BBox &b2);
	bool Overlaps(const BBox &b) const {
		bool x = (pMax.x >= b.pMin.x) && (pMin.x <= b.pMax.x);
		bool y = (pMax.y >= b.pMin.y) && (pMin.y <= b.pMax.y);
		bool z = (pMax.z >= b.pMin.z) && (pMin.z <= b.pMax.z);
		return (x && y && z);
	}

	BBox Union(const Point &p)const{
		BBox bbox;
		bbox.pMin=Point(min(pMin.x,p.x),
			min(pMin.y,p.y),
			min(pMin.z,p.z));
		bbox.pMax=Point(max(pMax.x,p.x),
			max(pMax.y,p.y),
			max(pMax.z,p.z));
		return bbox;
	}

	BBox Union(const BBox &b)const{
		BBox bbox;
		bbox.pMin=Point(min(pMin.x,b.pMin.x),
			min(pMin.y,b.pMin.y),
			min(pMin.z,b.pMin.z));
		bbox.pMax=Point(max(pMax.x,b.pMax.x),
			max(pMax.y,b.pMax.y),
			max(pMax.z,b.pMax.z));
		return bbox;
	}

	bool Inside(const Point &pt) const {
		return (pt.x >= pMin.x && pt.x <= pMax.x &&
	            pt.y >= pMin.y && pt.y <= pMax.y &&
	            pt.z >= pMin.z && pt.z <= pMax.z);
	}
	void Expand(float delta) {
		pMin -= Vector(delta, delta, delta);
		pMax += Vector(delta, delta, delta);
	}
	float Volume() const {
		Vector d = pMax - pMin;
		return d.x * d.y * d.z;
	}
	int MaximumExtent() const {
		Vector diag = pMax - pMin;
		if (diag.x > diag.y && diag.x > diag.z)
			return 0;
		else if (diag.y > diag.z)
			return 1;
		else
			return 2;
	}

  // HW4	
	BBox transform(float *matrix4x4) const{
		float p0in[4]={pMin.x,pMin.y,pMin.z,1.f};
		float p1in[4]={pMax.x,pMax.y,pMax.z,1.f};
		float p0out[4]={0.f,0.f,0.f,0.f};
		float p1out[4]={0.f,0.f,0.f,0.f};

		// matrix mult
		for(int i=0; i<4; i++)
			for(int j=0; j<4; j++){
				p0out[i]+=matrix4x4[i+4*j]*p0in[j];
				p1out[i]+=matrix4x4[i+4*j]*p1in[j];
			}		
		BBox out;

		// homogeneous divide
		for(int i=0; i<3; i++){
			p0out[i]/=p0out[3];
			p1out[i]/=p1out[3];
		}

		// make sure min is min, max is max
		out.pMin=Point(min(p0out[0],p1out[0]),min(p0out[1],p1out[1]),min(p0out[2],p1out[2]));
		out.pMax=Point(max(p0out[0],p1out[0]),max(p0out[1],p1out[1]),max(p0out[2],p1out[2]));

		return out;
	}

	void BoundingSphere(Point *c, float *rad) const;
	// BBox Public Data
	Point pMin, pMax;
};

#endif // _VPMATH_H
