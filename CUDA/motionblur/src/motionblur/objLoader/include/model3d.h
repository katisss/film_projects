#ifndef MODEL_3D_H
#define MODEL_3D_H

#include <stdlib.h>
#include <vector>
#include "vpmath.h"

// fwd decl
class objLoader;
class Material;
class GeometryList;

/* Somewhat general class for rendering quad and triangle mesh data with 
   vertex arrays (VBOs when supported) and fixed function shading.  
   Supports OBJ and heightfield input. Note that all features  aren't 
   implemented for OBJ support, just those necessary for the landspeeder 
   model, e.g., only quad meshes.  However it can easily be extended. 

   For anyone wanting to use or extend this framework, here are some other
   things to keep in mind:

  - OBJ files (including the landspeeder) have multiple texture maps for
    ambient, diffuse, and specular color; right now, I only use the
    ambient texture with the fixed-function pipeline.  You could switch
    to using shaders + multitexturing, similar to HW3, and use all color
    maps; this will require (1) adding Kd, Ks, Ka texture filename
    fields to the obj_material class in objLoader; (2) enabling
    multi-texturing and loading all three textures in the Material
    constructors; (3) switching to a shader pipeline (you could modify
    the HW3 shaders or roll your own).

    The same thing applies to bump maps included with OBJ files.

  - Due to a bug in objLoader, the files referenced in the OBJ file (MTL
    and texture) need to be in the application's working directory.

    Kevin Dale <dale@eecs.harvard.edu>
    cs175, Spring 2010
*/

#define COMPUTE_TERRAIN_NORMALS

class Model3D{
public:	
	/* create an empty model */
	Model3D() : ready(false),loader(NULL), geometry(NULL), materials(NULL){}
	~Model3D();
	/* initialize the model from the given OBJ filename */
	void loadOBJ(const char *filename);
	/* initialize the model from the given heightfield data */
	void loadHeightfield(const char *heightFilename, 
		const char *normalFilename, const char *textureFilename);
	/* render the model with vertex arrays (or VBOs if supported) */
	void render(bool usemat=1);
	/* returns the model's bounding box */
	const BBox & getBBox() const{return bbox;}
	/* returns the result of a bounding box test for OBJs and point-in-bbox test
     for heightfields */
	bool intersects(const BBox &bbox) const;
private:
	void cleanup();
	bool ready;
	objLoader *loader;
	Material **materials;
	GeometryList ***geometry;
	BBox bbox;
	int nm;
};

/* Stores a list of vertices and per-vertex data for parts of models 
   that share the same material and face type (triangles or quads) */
class GeometryList{
public:
	/* create an empty geometry list */
	GeometryList();
	/* create a geometry list for the given material and face type */
	GeometryList(objLoader *loader, int materialIndex, int vertsPerFace);
	/* create a geometry list for the given heightfield data and scale */
	GeometryList(const char *heightFilename, const char *normalFilename, 
		const char *colorFilename, const Vector &scale);
	~GeometryList();
	/* render the geometry list with vertex arrays (or VBOs if supported) */
	void render();
	/* returns the geometry list's bounding box */
	const BBox & getBBox() const{return bbox;}
	/* returns the result of a bounding box test for OBJs and point-in-bbox test
     for heightfields */
	bool intersects(const BBox &bbox) const;

private:
	// VBOs
	bool useVBOs;
	unsigned int vertexVBO,normalVBO,colorVBO,texCoordVBO;
	void setupVBOs();
	void teardownVBOs();
	
	int vertsPerFace, materialIndex, numFaces;
	bool hasTexture,hasColor, isHeightfield;
	int hw,hh;//heightfield only
	Vector scale;//heightfield only
	Point *vertexData;
	Vector *texCoordData;
	Vector *colorData;
	Vector *normalData;
	BBox bbox;
};

#endif