#ifndef MATERIAL_H
#define MATERIAL_H

#define GLEW_STATIC
#include <GL/glew.h>
#ifdef __MAC__
#	include <GLUT/glut.h>
#else
#  define FREEGLUT_STATIC
#  include <GL/glut.h>
#endif

#include <string>

/* Encapsulates a single material stored in an MTL file; note that
   this only supports a limited number of material properties and a 
   single texture (the Ka texture).  Additionally, the texture map
   must be a TGA image.

   Kevin Dale <dale@eecs.harvard.edu>
   cs175, Spring 2010   
*/

// fwd decl
struct obj_material;

#define MATERIAL_TEXTURE_UNIT        GL_TEXTURE0

class Material{
public:
	/* initialize the material with the given obj_material object*/
	Material(obj_material *objMaterial);
	/* initialize the material from a texture */
	Material(const char *textureFilename);
	/* create an empty material */
	Material();
	~Material();
	/* activate this material in OpenGL */
	void activate();
	/* deactivate this material in OpenGL */
	void deactivate();

private:

	// texture info
	bool hasTex;
	GLuint texHandle;
	GLint texWidth,texHeight,texComp;
	GLenum texFormat;

	// phong parameters
	float ambient[3];
	float diffuse[3];
	float specular[3];
	float specPower;

	// transparency
	float trans;
};

#endif