/* 
   Image space motion blur is a short cut to apply motion blur as a post process to an image of the scene based on
   object velocities.I am using OpenGl and GLSL to create the rendering and velocity data and CUDA for blurring by the
   vector and vbo objects for displaying the geometry.

   The basic steps are:
   1. render the scene to the frame buffer
   2. create a velocity map (x, y velocity in screen space, length in z channel)
   3. map the float velocity and colour texture so that its memory is accessible from CUDA
   4. run CUDA to blur the image based on velocity vectors
   5. copy result back
   6. display the texture with a full screen quad


*/

//#define DEBUG 
//#define DTIMER
////////
#ifdef _WIN32
#define WINDOWS_LEAN_AND_MEAN
#define NOMINMAX
#include <windows.h>
#pragma warning(disable:4996)
#endif

// Graphics includes
//#define GLEW_STATIC
#include <GL/glew.h>
#include <GL/glu.h>
#include <GL/glut.h>

#include <cutil_inline.h>
#include <cuda_runtime_api.h>
#include <cutil_inline.h>
#include <cutil_gl_inline.h>
#include <cutil_gl_error.h>

#include <cudaGL.h>

//ship
#include "model3d.h"
#include "blur.h"


//CUDA
dim3 block(16, 16, 1);
cudaArray *in_array, *in_Darray; 
cudaArray *texture_ptr;
unsigned int* out_data;

//////////////////DEBUG
bool enable_cuda = true;//
bool render_only = false;//
bool vel_only = false;//
bool depth_only = false;
bool cubePlane = true;
float numSamples = 32;
float blur_scale = .4;//0-.1
float cblur_scale = blur_scale*100.;

int avgMax = 50;
float res = 0;
int tCount = 0;
float tres = 0;
float overall = 0;
float runs = 0;

//ship
static const char *landspeederFilename = "./LandSpeeder.obj";
static Model3D landspeeder;

GLfloat modelView[16], prevModelView[16], camProj[16];
//vbo
GLuint vboID;
struct cudaGraphicsResource *cudaVbo_resource;
void *d_vbo_buffer = NULL;

///////////////////window
char cTitle[256];
unsigned int image_width = 512;
unsigned int image_height = 512;
unsigned int window_width = image_width;
unsigned int window_height = image_height;

float hwsv[3] = {float(window_width)/2., hwsv[1] = float(window_height)/2., 0.};
float aspect = (GLfloat)window_width /(GLfloat) window_height;

int iGLUTWindowHandle = 0;// handle to the GLUT window
GLuint kTextureDim = 12;
GLuint t1,t2;

// fbo variables
unsigned int* cuda_dest_resource;
GLuint shDrawTex, shDrawDistort, shDrawGLSLShader, shDrawVelCUDA;// programms
struct cudaGraphicsResource *cuda_tex_result_resource, *cuda_tex_screen_resource, *cuda_tex_vel_resource;

GLuint fbo_source;
unsigned int num_texels = image_width * image_height;
unsigned int num_values = num_texels * 4;
int size_tex_data = sizeof(GLubyte) * num_values;

// (offscreen) render target fbo variables
GLuint framebuffer; 
GLuint depth_buffer;      // to bind the proper targets
GLuint tex_screen; 
GLuint vel_screen; 
GLuint velocity_tex;      // where we render the image
GLuint tex_cudaResult;    // where we will copy the CUDA result

float rotate[3];
bool animate = true;

// Timer
static int fpsCount = 0;
//static int fpsLimit = 1;
unsigned int timer = 0;
unsigned int dbgtimer = 0;

// shader parameters 
#ifndef USE_TEXTURE_RGBA8UI
#pragma message("Note: Using Texture fmt GL_RGBA16F_ARB")
#else
// NOTE: the current issue with regular RGBA8 internal format of textures
// is that HW stores them as BGRA8. Therefore CUDA will see BGRA where users
// expected RGBA8. To prevent this issue, the driver team decided to prevent this to happen
// instead, use RGBA8UI which required the additional work of scaling the fragment shader
// output from 0-1 to 0-255. This is why we have some GLSL code, in this case
#   pragma message("Note: Using Texture RGBA8UI + GLSL for teapot rendering")
#endif


// ----------------------------------------------------------------------------------------------------
// Fixed function lighting
// ----------------------------------------------------------------------------------------------------
void enableLights(){
   glEnable(GL_LIGHTING);

    float lightDir[4] = {100.f, -1.f, 10.f, 1.f};
    float nopos[4] = {7.f, 0.f, 0.f, 0.f};
    float white[4] = {0.4f, 0.4f, 0.4f, 1.f};
    float brightWhite[3] = {0.8f, 0.8f, 0.8f};

    // ambient light source
     glLightModelfv(GL_LIGHT_MODEL_AMBIENT, white);

    // white directional light source
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();

    glEnable(GL_LIGHT1);
    glLightfv(GL_LIGHT1, GL_POSITION, nopos);
    glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, lightDir);
    glLightfv(GL_LIGHT1, GL_AMBIENT, white);
    glLightfv(GL_LIGHT1, GL_DIFFUSE, white);
    glLightfv(GL_LIGHT1, GL_SPECULAR, white);
    glPopMatrix();
}



////////////////////////////////////////////////////////////////////////////////
void renderScene(unsigned int velDraw)
{
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
   glEnable(GL_DEPTH_TEST);   ///
   GLint location1;

   if(velDraw == 0)
       glUseProgram(0);
    else if(velDraw == 1)//CUDA velocity map
    {
        glUseProgram(shDrawVelCUDA);
      GLint pmv = glGetUniformLocation(shDrawVelCUDA, "prevModelView");
      glUniformMatrix4fv(pmv, 1, 0, prevModelView);

      GLint hws = glGetUniformLocation(shDrawVelCUDA, "halfWindowSize");
      glUniform3fv(hws, 1, hwsv);
#ifdef DEBUG 
      printf("cs: modelView: %d, %.3f, %.3f, %.3f\n", mv,  blur_scale,  modelView[1],  modelView[2]);
      printf("cs:prevModelView: %d, %.3f, %.3f, %.3f\n\n", pmv, prevModelView[0], prevModelView[1], prevModelView[2]);
#endif
        CUT_CHECK_ERROR_GL2();

    }
   else if(velDraw == 2) // Blur GLSL shader, bind beauty texture
    {
      //texture
      glActiveTexture(GL_TEXTURE0);
      glBindTexture(GL_TEXTURE_2D, tex_screen);

        glUseProgram(shDrawGLSLShader);
      location1 = glGetUniformLocation(shDrawGLSLShader, "sceneTex");
       //Bind to tex unit 0
      glUniform1i(location1, 0);

      GLint pmv = glGetUniformLocation(shDrawGLSLShader, "prevModelView");
      glUniformMatrix4fv(pmv, 1, 0, prevModelView);
      GLint mv = glGetUniformLocation(shDrawGLSLShader, "modelView");
      glUniformMatrix4fv(mv, 1, 0, modelView);

#ifdef DEBUG 
      printf("cp: %d %.2f, %.2f, %.2f, %.2f\n", cp, camProj[0],camProj[1], camProj[2],camProj[3]);
      printf("cp: %d %.2f, %.2f, %.2f, %.2f\n", cp, camProj[4],camProj[5], camProj[6],camProj[7]);
      printf("cp: %d %.2f, %.2f, %.2f, %.2f\n", cp, camProj[8],camProj[9], camProj[10],camProj[11]);
      printf("cp: %d %.2f, %.2f, %.2f, %.2f\n\n", cp, camProj[12],camProj[13], camProj[14],camProj[15]);
#endif
      GLint hwsl = glGetUniformLocation(shDrawGLSLShader, "halfWindowSize");
      glUniform3fv(hwsl, 1, hwsv);

      GLint sll = glGetUniformLocation(shDrawGLSLShader, "samples");
      glUniform1fv(sll, 1, (const GLfloat *)&numSamples);

      GLint bs = glGetUniformLocation(shDrawGLSLShader, "blurScale");
      glUniform1fv(bs, 1, (const GLfloat *)&blur_scale);
#ifdef DEBUG 
      printf("bs:     modelView: %d, %d, %d, %.3f\n", location1,  mv,  shDrawGLSLShader,  numSamples);
      printf("bs: prevModelView: %d, %d, %d, %.3f\n\n", sll, hwsl, bs, blur_scale);
#endif
      glBindFragDataLocationEXT(shDrawGLSLShader, 0, "FragColor");

        CUT_CHECK_ERROR_GL2();
   }
   else if(velDraw == 3) // Beauty
    {
      glEnable(GL_LIGHTING);
      glEnable(GL_LIGHT0);
        glUseProgram(shDrawDistort);
      GLint mv = glGetUniformLocation(shDrawDistort, "modelView");
      glUniformMatrix4fv(mv, 1, 0, modelView);

      GLint pmv = glGetUniformLocation(shDrawDistort, "prevModelView");
      glUniformMatrix4fv(pmv, 1, 0, prevModelView);

      GLint bs = glGetUniformLocation(shDrawDistort, "blurScale");
      glUniform1fv(bs, 1, (const GLfloat *)&blur_scale);
#ifdef DEBUG 
      printf("beauty:  blurScale: %d, %.3f, %.3f, %.3f\n", mv,  blur_scale,  modelView[1],  modelView[2]);
      printf("beauty: prevModelView: %d, %.3f, %.3f, %.3f\n\n", pmv, prevModelView[0], prevModelView[1], prevModelView[2]);
#endif
      //glBindFragDataLocationEXT(shDrawDistort, 0, "FragColor");

        CUT_CHECK_ERROR_GL2();
   }
    glMatrixMode(GL_MODELVIEW);
   glPushMatrix();
   glLoadIdentity();

   gluPerspective(130.0, (GLfloat)window_width /(GLfloat) window_height, 0.1, 15.0);// near far
   //glGetFloatv(GL_PROJECTION_MATRIX, (float *) &camProj[0]);

   glTranslatef(0.0, 0.0, -2.0);
   glRotatef(rotate[0], 1.0, 0.0, 0.0);
    glRotatef(rotate[1], 0.0, 1.0, 0.0);
    glRotatef(rotate[2], 0.0, 0.0, 1.0);

   ////////// render from the vbo
   if (cubePlane == 1)
   {
      glPushMatrix();
      landspeeder.render(0);
      glPopMatrix();

   }
   else
   {
      glBindBufferARB(GL_ARRAY_BUFFER_ARB, vboID);
      // enable vertex arrays
      glEnableClientState(GL_NORMAL_ARRAY);
      glEnableClientState(GL_VERTEX_ARRAY);

      // before draw, specify vertex and index arrays with their offsets
      glNormalPointer(GL_FLOAT, 0, (void*)(sizeof(float)*24));
      glVertexPointer(3, GL_FLOAT, 0, 0);

      glDrawArrays(GL_QUADS, 0, 24);

      glDisableClientState(GL_VERTEX_ARRAY);  // disable vertex arrays
      glDisableClientState(GL_NORMAL_ARRAY);
      glBindBufferARB(GL_ARRAY_BUFFER_ARB, 0);

   }
    if (velDraw != 0)
      glGetFloatv(GL_MODELVIEW_MATRIX, (float *) modelView);

   glPushMatrix();
   glTranslatef(1.0, 0.0, .0);
    glutSolidTeapot(.1);///////
   glPopMatrix();

   glPopMatrix();

   //clean
   if (velDraw != 0)
    {
      for (int i = 0;i<16;i++)/////////////////
         prevModelView[i] = modelView[i];
   }
   glUseProgram(0);
   glBindTexture(0,0);
   if(velDraw == 2)
      glUniform1iARB(location1, 0);

    CUT_CHECK_ERROR_GL2();
}

// copy image and processCUDAImgKer using CUDA
void processImageCUDA(float blurScale)
{
    out_data = cuda_dest_resource;

    cutilSafeCall(cudaGraphicsMapResources(1, &cuda_tex_screen_resource, NULL));///
    cutilSafeCall(cudaGraphicsMapResources(1, &cuda_tex_vel_resource, 0));

    cutilSafeCall(cudaGraphicsSubResourceGetMappedArray(&in_array, cuda_tex_screen_resource, 0, 0));///
    cutilSafeCall(cudaGraphicsSubResourceGetMappedArray(&in_Darray, cuda_tex_vel_resource, 0, 0));///

    // calculate grid size
    dim3 grid(image_width / block.x, image_height/ block.y, 1);
    int sbytes = 0;

    //////////// execute CUDA kernel
   //int r = 23;
   //int sbytes = (block.x+(2*r))*(block.y+(2*r))*sizeof(float);
    launch_cudaProcess(grid, block, sbytes, in_Darray,
                       in_array, out_data, image_width, image_height,  
                       block.x + (2*1), cblur_scale, numSamples);


    cutilSafeCall(cudaGraphicsUnmapResources(1, &cuda_tex_vel_resource, 0));
    cutilSafeCall(cudaGraphicsUnmapResources(1, &cuda_tex_screen_resource, 0));

   // result
    cutilSafeCall(cudaGraphicsMapResources(1, &cuda_tex_result_resource, 0));
    cutilSafeCall(cudaGraphicsSubResourceGetMappedArray(&texture_ptr, cuda_tex_result_resource, 0, 0));

    cutilSafeCall(cudaMemcpyToArray(texture_ptr, 0, 0, cuda_dest_resource, size_tex_data, cudaMemcpyDeviceToDevice));
    cutilSafeCall(cudaGraphicsUnmapResources(1, &cuda_tex_result_resource, 0));
}

// display image to the screen as textured quad
void displayImage(GLuint texture)
{
    glBindTexture(GL_TEXTURE_2D, texture);
    glEnable(GL_TEXTURE_2D);
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_LIGHTING);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

    glMatrixMode(GL_PROJECTION);

    glPushMatrix();//save
    glLoadIdentity();
    glOrtho(-1.0, 1.0, -1.0, 1.0, -1.0, 1.0);

    glMatrixMode( GL_MODELVIEW);
    glLoadIdentity();

    glViewport(0, 0, window_width, window_height);

    glUseProgram(shDrawTex);
    GLint id = glGetUniformLocation(shDrawTex, "texImage");
    glUniform1i(id, 0); // texture unit 0 to "texImage"
    CUT_CHECK_ERROR_GL2();

    glBegin(GL_QUADS);
    glTexCoord2f(0.0, 0.0); glVertex3f(-1.0, -1.0, 0.5);
    glTexCoord2f(1.0, 0.0); glVertex3f(1.0, -1.0, 0.5);
    glTexCoord2f(1.0, 1.0); glVertex3f(1.0, 1.0, 0.5);
    glTexCoord2f(0.0, 1.0); glVertex3f(-1.0, 1.0, 0.5);
    glEnd();

    glMatrixMode(GL_PROJECTION);
    glPopMatrix();//load

    glDisable(GL_TEXTURE_2D);

    glUseProgram(0);

    CUT_CHECK_ERROR_GL2();
}



void display()
{
    cutStartTimer(timer);
    if (render_only == 1)
    {
      renderScene(3);
    }
    else if (vel_only == 1)
    {
      renderScene(1);
    }
    if (render_only == 0 && vel_only == 0)
    {
        if (enable_cuda == 1)
        {
#ifdef DTIMER
         cutResetTimer(timer);
          cutStartTimer(timer);
#endif
         glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, framebuffer);
         glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, tex_screen, 0);
         renderScene(3);
#ifdef DTIMER
         cutStopTimer(timer);
         float res = cutGetAverageTimerValue(timer);
         if (tCount == 0)
            printf("beauty: %.5f\n", res);
         tres += res;
         cutResetTimer(timer);
         cutStartTimer(timer);
#endif
         // switch texture
         glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, velocity_tex, 0);
         renderScene(1);
         glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
#ifdef DTIMER
         cutStopTimer(timer);
         res = cutGetAverageTimerValue(timer);
         if (tCount == 0)
            printf("vel: %.5f\n", res);
         tres += res;
         cutResetTimer(timer);
         cutStartTimer(timer);
#endif
         processImageCUDA(blur_scale);
#ifdef DTIMER
         cutStopTimer(timer);
         res = cutGetAverageTimerValue(timer);
         if (tCount == 0)
            printf("blur: %.5f\n", res);
         tres += res;
         cutResetTimer(timer);
          cutStartTimer(timer);
#endif
         displayImage(tex_cudaResult);
#ifdef DTIMER
         cutStopTimer(timer);
         res = cutGetAverageTimerValue(timer);
         if (tCount == 0)
            printf("displayImage: %.5f\n", res);
         tres += res;
         if (++tCount == avgMax)
         {
            printf("CUDA total: %.5f\n\n", tres/ avgMax);
            tCount = 0;
            tres = 0;
         }
         cutResetTimer(timer);
#endif
      }
      else //GLSL shader
      {
#ifdef DTIMER
         float tres = 0;
         cutResetTimer(timer);
         cutStartTimer(timer);
#endif
         glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, framebuffer);
         glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, tex_screen, 0);
         //glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_RENDERBUFFER_EXT, depth_buffer);
         renderScene(3);
         glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
#ifdef DTIMER
         cutStopTimer(timer);
         float res = cutGetAverageTimerValue(timer);
         tres += res;
         if (tCount == 0)
            printf("GLSL beauty: %.5f\n", res);

         cutResetTimer(timer);
         cutStartTimer(timer);
#endif
         renderScene(2);

#ifdef DTIMER
         cutStopTimer(timer);
         res = cutGetAverageTimerValue(timer);
         tres += res;
         printf("GLSL blur: %.5f\n", res);
         if (++tCount == avgMax)
         {
            printf("GLSL total: %.5f\n\n", tres/ avgMax);
            tCount = 0;
            tres = 0;
         }
#endif
      }
   }

   cutStopTimer(timer);
   glutSwapBuffers();

   // Update fps counter, fps/title display and log
   res +=  cutGetAverageTimerValue(timer);
   if (++fpsCount == avgMax)
   {
       float fps = 1000.0f /(res/avgMax);
      overall += fps;
      runs += 1.;
      if (enable_cuda)
         sprintf(cTitle, "CUDA GL Post Processing (%.f , %.3f): %.1f fps, average: %.1f fps", numSamples, blur_scale, fps, overall/runs);
      else
         sprintf(cTitle, "GLSL Post Processing (%.f , %.3f): %.1f fps, average: %.1f fps", numSamples, blur_scale, fps, overall/runs);
      if (render_only)
         sprintf(cTitle, "RENDER ONLY  (%.f , %.3f): %.1f fps", numSamples, blur_scale, fps);
      if (vel_only)
         sprintf(cTitle, "VELOCITY ONLY  (%.f , %.3f): %.1f fps", numSamples, blur_scale, fps); ;
        glutSetWindowTitle(cTitle);

        fpsCount = 0; 
        cutResetTimer(timer);  
      res = 0;
    }
}

void idle()
{
    if (animate) {
        rotate[0] += 0.2;
        rotate[1] += 0.6;
        rotate[2] += 1.0;
    }
    glutPostRedisplay();
}

void resettimer()
{
      overall = 0.;
      res = 0.;
      runs = 0.;
}

////////////////////////////////////////////////////////////////////////////////
void keyboard(unsigned char key, int /*x*/, int /*y*/)
{
    switch(key) {
    case(27) :
        Cleanup(EXIT_SUCCESS);
   case ' ':
   {
        enable_cuda ^= 1;
        resettimer();
        break;
   }
   case 'a':
        animate ^= 1;
        break;
   case 'c':
        cubePlane ^= 1;
        break;
   case 'r':
   {
        render_only ^= 1;
        vel_only = 0;
   }
        break;
   case 'v':
   {
        vel_only ^= 1;
        render_only = 0;
        break;
   }
  case 'm':
    if (numSamples <= 128.)
    {
       numSamples += 16;
      setParam(numSamples);
    }
   break;
  case 'n':
     if (numSamples >= 16.)
     {
      numSamples -= 16;
      setParam(numSamples);
     }
  case 't':
   {
      resettimer();
   }
  case '+':
    if (blur_scale < 1.6)
    {
      blur_scale += 0.05;
      cblur_scale = blur_scale*100.;
    }
    break;

  case '-':
    if (blur_scale >= .0)
    {
      blur_scale -= 0.05;
      cblur_scale = blur_scale*100.;
    }
    break;
    }
}


void initMenus(void)
{
   glutCreateMenu(mainMenu);

   glutAddMenuEntry("Toggle CUDA or GLSL  [space bar]", ' ');
   glutAddMenuEntry("Animate [a]", 'a');
   glutAddMenuEntry("Toggle cube/ship [c]", 'c');
   glutAddMenuEntry("Render only [r]", 'r');
   glutAddMenuEntry("Velocity only [v]", 'v');
   glutAddMenuEntry("Decrease number of samples [n]", 'n');
   glutAddMenuEntry("Increase number of samples [m]", 'm');
   glutAddMenuEntry("Increase blur_scale [+]", '+');
   glutAddMenuEntry("Decrease blur_scale [-]", '-');
   glutAddMenuEntry("Reset timer [t]", 't');

   glutAttachMenu(GLUT_RIGHT_BUTTON);
}


void reshape(int w, int h)
{
    window_width = w;
    window_height = h;
}

void mainMenu(int i)
{
    keyboard((unsigned char) i, 0, 0);
}


/////depth
void createTextureDSrc(GLuint* velocity_tex, unsigned int size_x, unsigned int size_y)
{
    // create a texture
    glGenTextures(1, velocity_tex);
    glBindTexture(GL_TEXTURE_2D, *velocity_tex);

    // set basic parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    // buffer data

    printf("Creating a Texture render target GL_RGBA16F_ARB\n");
   // glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F_ARB, size_x, size_y, 0, GL_RGBA, GL_FLOAT, NULL);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F_ARB, size_x, size_y, 0, GL_RGBA, GL_FLOAT, NULL);

    CUT_CHECK_ERROR_GL2();
    // register this texture with CUDA
   cutilSafeCall(cudaGraphicsGLRegisterImage(&cuda_tex_vel_resource, *velocity_tex, 
                          GL_TEXTURE_2D, cudaGraphicsMapFlagsReadOnly));
}

////////////////////////////////////////////////////////////////////////////////
void createTextureSrc(GLuint* tex_screen, unsigned int size_x, unsigned int size_y)
{
    // create a texture
    glGenTextures(1, tex_screen);
    glBindTexture(GL_TEXTURE_2D, *tex_screen);

    // set basic parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    // buffer data
    printf("Creating a Texture render target GL_RGBA16F_ARB\n");
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F_ARB, size_x, size_y, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);

    CUT_CHECK_ERROR_GL2();
    // register this texture with CUDA
    cutilSafeCall(cudaGraphicsGLRegisterImage(&cuda_tex_screen_resource, *tex_screen, 
                          GL_TEXTURE_2D, cudaGraphicsMapFlagsReadOnly));
}

////////////////////////////////////////////////////////////////////////////////
void createTextureDst(GLuint* tex_cudaResult, unsigned int size_x, unsigned int size_y)
{
    // create a texture
    glGenTextures(1, tex_cudaResult);
    glBindTexture(GL_TEXTURE_2D, *tex_cudaResult);

    // set basic parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8UI_EXT, size_x, size_y, 0, GL_RGBA_INTEGER_EXT, GL_UNSIGNED_BYTE, NULL);
    CUT_CHECK_ERROR_GL2();
    // register this texture with CUDA
    cutilSafeCall(cudaGraphicsGLRegisterImage(&cuda_tex_result_resource, *tex_cudaResult, 
                          GL_TEXTURE_2D, cudaGraphicsMapFlagsWriteDiscard));
}

////////////////////////////////////////////////////////////////////////////////
void deleteTexture(GLuint* tex)
{
    glDeleteTextures(1, tex);
    CUT_CHECK_ERROR_GL2();

    *tex = 0;
}

////////////////////////////////////////////////////////////////////////////////
/*
    m_depthTex = createTexture(GL_TEXTURE_2D, m_imageW, m_imageH, GL_DEPTH_COMPONENT24_ARB, GL_DEPTH_COMPONENT);
    m_imageFbo->AttachTexture(GL_TEXTURE_2D, m_depthTex, GL_DEPTH_ATTACHMENT_EXT);
   */
////////////////////////////////////////////////////////////////////////////////

void createDepthBuffer(GLuint* depth, unsigned int size_x, unsigned int size_y)
{
    // create a renderbuffer
    glGenRenderbuffersEXT(1, depth);
    glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, *depth);

    // allocate storage
    glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT, GL_DEPTH_COMPONENT24, size_x, size_y);

    // clean up
    glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, 0);

    CUT_CHECK_ERROR_GL2();
}


////////////////////////////////////////////////////////////////////////////////
void deleteDepthBuffer(GLuint* depth)
{
    glDeleteRenderbuffersEXT(1, depth);
    CUT_CHECK_ERROR_GL2();

    *depth = 0;
}



////////////////////////////////////////////////////////////////////////////////
void createFramebuffer(GLuint* fbo, GLuint color, GLuint depth)
{
    // create and bind a framebuffer
    glGenFramebuffersEXT(1, fbo);
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, *fbo);

    // attach images
    glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, color, 0);
    glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_RENDERBUFFER_EXT, depth);
    // clean up
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);

    CUT_CHECK_ERROR_GL2();
}



////////////////////////////////////////////////////////////////////////////////
void deleteFramebuffer( GLuint* fbo)
{
    glDeleteFramebuffersEXT(1, fbo);
    CUT_CHECK_ERROR_GL2();

    *fbo = 0;
}

////////////////////////////////////////////////////////////////////////////////
int main(int argc, char** argv)
{
    printf("%s Starting...\n\n", argv[0]);

    animate         = true;
    runProgramm(argc, argv);
    
//    shrEXIT(argc, (const char**)argv);
}

////////////////////////////////////////////////////////////////////////////////
void Cleanup(int iExitCode)
{
    cudaThreadExit();
    if(iGLUTWindowHandle)glutDestroyWindow(iGLUTWindowHandle);

    // finalize logs and leave
    {
        printf("postProcessGL.exe Exiting...\nPress <Enter> to Quit\n");
        #ifdef WIN32
            getchar();
        #endif
    }
   //cleanup()
//   deleteCudaVBO(&vbo, cuda_Vbo_resource);
    exit (iExitCode);
}


////////////////////////////////////////////////////////////////////////////////
GLuint compileGLSLprogram(const char *vertex_shader_src, const char *fragment_shader_src)
{
  GLuint v, f, p = 0;

  p = glCreateProgram();
    
  if (vertex_shader_src) {
      v = glCreateShader(GL_VERTEX_SHADER);
      glShaderSource(v, 1, &vertex_shader_src, NULL);
      glCompileShader(v);

      // check if shader compiled
      GLint compiled = 0;
      glGetShaderiv(v, GL_COMPILE_STATUS, &compiled);
      
      if (!compiled)
      {
          //#ifdef NV_REPORT_COMPILE_ERRORS
          char temp[256] = "";
          glGetShaderInfoLog( v, 256, NULL, temp);
          printf("Vtx Compile failed:\n%s\n", temp);
          //#endif
          glDeleteShader( v);
          return 0;
      }
      else
      glAttachShader(p,v);
  }
  
  if (fragment_shader_src)  {
      f = glCreateShader(GL_FRAGMENT_SHADER);
      glShaderSource(f, 1, &fragment_shader_src, NULL);
      glCompileShader(f);

      // check if shader compiled
      GLint compiled = 0;
      glGetShaderiv(f, GL_COMPILE_STATUS, &compiled);
      
      if (!compiled)
      {
          char temp[256] = "";
          glGetShaderInfoLog(f, 256, NULL, temp);
          printf("frag Compile failed:\n%s\n", temp);
          glDeleteShader(f);
          return 0;
      }
      else
          glAttachShader(p,f);
  }
  
  glLinkProgram(p);

  int infologLength = 0;
  int charsWritten  = 0;
  
  glGetProgramiv(p, GL_INFO_LOG_LENGTH, (GLint *)&infologLength);
  
  if (infologLength > 0) {
      char *infoLog = (char *)malloc(infologLength);
      glGetProgramInfoLog(p, infologLength, (GLsizei *)&charsWritten, infoLog);
      printf("Shader compilation error: %s\n", infoLog);
      free(infoLog);
  }

  return p;
}

////////////////////////////////////////////////////////////////////////////////
void initCUDABuffers()
{
    // set up vertex data parameter
    setParam(numSamples);
    size_tex_data = sizeof(GLubyte) * num_values;
    cutilSafeCall(cudaMalloc((void**)&cuda_dest_resource, size_tex_data));
}


////////////////////////////////////////////////////////////////////////////////
void initGLBuffers()
{
    // create texture that will receive the result of CUDA
    createTextureDst(&tex_cudaResult, image_width, image_height);

    // create texture for splitting onto the screen
    createTextureSrc(&tex_screen, image_width, image_height);
    createTextureDSrc(&velocity_tex, image_width, image_height);////////
    
    // create a depth buffer for off screen rendering
    createDepthBuffer(&depth_buffer, image_width, image_height);
    
    // create a frame buffer for off screen rendering
    createFramebuffer(&framebuffer, tex_screen, depth_buffer);

    // load shader programs
    shDrawTex = compileGLSLprogram(glsl_drawtex_vertshader_src, glsl_drawtex_fragshader_src);
    shDrawGLSLShader = compileGLSLprogram(glsl_vel_GLSLvertshader_src, glsl_velGLSL_fragshader_src);
    shDrawVelCUDA = compileGLSLprogram(glsl_vel_CUDAvertshader_src, glsl_velCUDA_fragshader_src);
    shDrawDistort = compileGLSLprogram(glsl_beauty_vertshader_src, glsl_beauty_fragshader_src);
    CUT_CHECK_ERROR_GL2();
}

////////////////////////////////////////////////////////////////////////////////
void runProgramm(int argc, char** argv)
{
    // First initialize OpenGL context, so we can properly set the GL for CUDA.
    // This is necessary in order to achieve optimal performance with OpenGL/CUDA interop.
    if( false == initGL(argc, argv))
    {
        return;
    }

    // Now initialize CUDA context (GL context has been created already)
    initCUDA(argc, argv, true);
    
    cutCreateTimer(&timer);
    cutResetTimer(timer);  
   // create VBO
   //createVBO(&vboID, &cudaVbo_resource, cudaGraphicsMapFlagsWriteDiscard);

    // register callbacks
    glutDisplayFunc(display);
    glutKeyboardFunc(keyboard);
    glutReshapeFunc(reshape);
    glutIdleFunc(idle);

    initGLBuffers();
    initCUDABuffers();

   landspeeder = Model3D();
   landspeeder.loadOBJ(landspeederFilename);

    // start rendering main loop
    glutMainLoop();

    Cleanup(EXIT_FAILURE);
}


//! Initialize CUDA context
bool initCUDA( int argc, char **argv, bool bUseGL)
{
    if (bUseGL) {
        if ( cutCheckCmdLineFlag(argc, (const char **)argv, "device"))
            cutilGLDeviceInit(argc, argv);
        else {
            cudaGLSetGLDevice (cutGetMaxGflopsDeviceId());
        }
    } else {
        if ( cutCheckCmdLineFlag(argc, (const char **)argv, "device"))
            cutilDeviceInit(argc, argv);
        else {
            cudaSetDevice (cutGetMaxGflopsDeviceId());
        }
    }

    return true;
}

////////////////////////////////////////////////////////////////////////////////
bool initGL(int argc, char **argv)
{
    // Create GL context
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGBA | GLUT_ALPHA | GLUT_DOUBLE | GLUT_DEPTH);
    glutInitWindowSize(window_width, window_height);
    iGLUTWindowHandle = glutCreateWindow("CUDA OpenGL post-processing");

    // initialize necessary OpenGL extensions
    glewInit();
    if (! glewIsSupported(
        "GL_VERSION_2_0 " 
        "GL_ARB_pixel_buffer_object "
        "GL_EXT_framebuffer_object "
       )) {
        printf("ERROR: Support for necessary OpenGL extensions missing.");
        fflush(stderr);
        return CUTFalse;
    }

    // default initialization
    glClearColor(0., 0.02, 0.1, .0);

    // viewport
    glViewport(0, 0, window_width, window_height);

    // projection
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(130.0, (GLfloat)window_width / (GLfloat) window_height, 0.1, 15.0);
    glGetFloatv(GL_PROJECTION_MATRIX, (float *) &camProj[0]);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);


    glShadeModel(GL_SMOOTH);
    CUT_CHECK_ERROR_GL2();

    // checker
    enableLights();
    initMenus();

    return CUTTrue;
}


