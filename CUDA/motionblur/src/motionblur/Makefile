# Makefile for CS 264 

CCFILES =main.cpp cudaVbo.cpp GLTools.cpp model3d.cpp obj_parser.cpp objLoader.cpp string_extra.cpp \
GLTriangleBatch.cpp math3d.cpp GLBatch.cpp GLShaderManager.cpp 

CUFILES = motionBlur.cu

CC_DEPS  := $(wildcard *.hpp)
CU_DEPS  := $(wildcard *.hpp) Makefile


EXECUTABLE = motionblur 

ROOTDIR := .
ROOTOBJDIR ?= $(ROOTDIR)/../obj/x86_64/release
ROOTBINDIR ?= $(ROOTDIR)/../bin
LIBDIR     := $(ROOTDIR)/../common/lib/linux
BINDIR=./bin
#SRCDIR = ./linuxsrc/
OBJDIR = ../obj/x86_64/release

CUDA_INSTALL_PATH ?= /opt/cuda-3.2
CUDA_SDK_INSTALL_PATH ?= /opt/cudasdk-3.2/C

# Compilers
NVCC       := $(CUDA_INSTALL_PATH)/bin/nvcc 
CXX        := mpic++
LINK       := mpic++ -fPIC

# Includes
INCLUDES  += -I. -I/opt/cudasdk-3.2/C/common/inc/GL -I$(CUDA_INSTALL_PATH)/include -I$(CUDA_SDK_INSTALL_PATH)/common/inc \
	-I$(CUDA_SDK_INSTALL_PATH)/common/shared/inc -I/opt/cudasdk-3.2/C/common/inc \
	-I./Libraries/GLTools/include/GL -I./Libraries/GLTools/include $(CUDA_INC_PATH)\
	-I./objLoader/include -I/opt/cudasdk-3.2/C/common/lib/linux

# Warning flags
CXXWARN_FLAGS := \
        -W -Wall -Wextra -Weffc++ \
        -Wimplicit \
        -Wswitch \
        -Wformat \
        -Wchar-subscripts \
        -Wparentheses \
        -Wmultichar \
        -Wtrigraphs \
        -Wpointer-arith \
        -Wcast-align \
        -Wreturn-type \
        -Wno-unused-function \
        $(SPACE)



# Compiler-specific flags
NVCCFLAGS := --compiler-options -W,-Wall,-Wextra
CXXFLAGS  := $(CXXWARN_FLAGS)

# Common flags
COMMONFLAGS += $(INCLUDES) -DUNIX

ifeq ($(dbg),1)
   COMMONFLAGS += -g
   NVCCFLAGS   += -D_DEBUG
else
   COMMONFLAGS += -O2
endif

# Libs cudart.lib cutil32.lib shrUtils32.lib glew32s.lib
LIB := -L/opt/cudasdk-3.2/C/lib/ -L/opt/cudasdk-3.2/C/common/lib/linux -L$(CUDA_INSTALL_PATH)/lib64 \
-L$(CUDA_SDK_INSTALL_PATH)/lib -lcudart -lcutil_x86_64 -L/opt/cudasdk-3.2/shared/lib -L$(CUDASDK_HOME)/shared/lib/linux \
-lGLEW_x86_64 -lglut -lGL -lGLU -lX11 -lXi -lXmu -lm

LIB_ARCH=x86_64
#STATIC_LIB := libcutil.a 
#STATIC_LIB := libcudart.a 


TARGET    := $(BINDIR)/$(EXECUTABLE)

# Add common flags
NVCCFLAGS += $(COMMONFLAGS)
CXXFLAGS  += $(COMMONFLAGS)


#####################
# Set up object files

OBJS += $(patsubst %.cpp,$(OBJDIR)/%.cpp.o,$(notdir $(CCFILES)))
OBJS += $(patsubst %.cu,$(OBJDIR)/%.cu.o,$(notdir $(CUFILES)))

	
#####################
# Rules

# Lib/exe configuration
ifneq ($(STATIC_LIB),)
	TARGETDIR := $(LIBDIR)
	TARGET   := $(subst .a,_$(LIB_ARCH)$(LIBSUFFIX).a,$(LIBDIR)/$(STATIC_LIB))
	LINKLINE  = ar rucv $(TARGET) $(OBJS)
else
	ifneq ($(OMIT_CUTIL_LIB),1)
		LIB += -lcutil_$(LIB_ARCH)$(LIBSUFFIX) -lshrutil_$(LIB_ARCH)$(LIBSUFFIX)
	endif

	TARGETDIR := $(BINDIR)/$(BINSUBDIR)
	TARGET    := $(TARGETDIR)/$(EXECUTABLE)
	LINKLINE  = $(LINK) -o $(TARGET) $(OBJS) $(LIB)
endif


$(OBJDIR)/%.cpp.o : $(SRCDIR)%.cpp $(CC_DEPS)
	$(CXX) $(CXXFLAGS) -o $@ -c $<

$(OBJDIR)/%.cu.o : $(SRCDIR)%.cu $(CU_DEPS)
	$(NVCC) $(NVCCFLAGS) $(SMVERSIONFLAGS) -o $@  -c ./motionBlur.cu
# $<


$(TARGET) : $(OBJDIR)/.exist $(BINDIR)/.exist $(OBJS)
	$(LINK) -o $(TARGET) $(OBJS) $(LIB)


$(OBJDIR)/.exist:
	mkdir -p $(OBJDIR)
	touch $(OBJDIR)/.exist

$(BINDIR)/.exist:
	mkdir -p $(BINDIR)
	touch $(BINDIR)/.exist
	@echo "all cleaned up!"
	
tidy:
	find . | egrep "#" | xargs rm -f
	find . | egrep "\~" | xargs rm -f

clean: tidy
	-rm -f $(OBJS)
	-rm -f $(TARGET)

clobber: clean
	-rm -rf $(OBJDIR)
	-rm -rf $(BINDIR)
