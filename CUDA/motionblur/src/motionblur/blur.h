
#ifndef CVBO_H
#define CVBO_H

extern GLuint vboID;
extern struct cudaGraphicsResource *cudaVbo_resource;
extern void *d_vbo_buffer;
extern GLfloat vertices[];
extern GLfloat normals[];
extern  unsigned int vertSize;
extern float rotate[3];

extern GLfloat projection[16];
extern GLfloat modelView[16]; 
extern GLfloat prevModelView[16];
//Matrix d_A,d_B,d_C;
extern unsigned int window_width;
extern unsigned int window_height;

void createVBO(GLuint* vboID, struct cudaGraphicsResource **vbo_res, 
       unsigned int vbo_res_flags);

void deleteVBO(GLuint* vboID, struct cudaGraphicsResource *vbo_res);
void runCuda(struct cudaGraphicsResource **vbo_resource);


void renderScene(unsigned int vel);

////////////////////////////// CUDA
extern void setParam(float numSamples); 
extern "C" void launch_cudaProcess( dim3 grid, dim3 block, int sbytes, cudaArray *g_Ddata,
            cudaArray *g_data, unsigned int* g_odata, 
            int imgw, int imgh, int tilew,  float highlight, float numSamples);


void runProgramm(int argc, char** argv);
void Cleanup(int iExitCode);

//////// GL functionality
void initMenus(void);
bool initCUDA( int argc, char **argv, bool bUseGL);
bool initGL(int argc, char** argv);

void createTextureDst(GLuint* tex_cudaResult, unsigned int size_x, unsigned int size_y);
void createTextureSrc(GLuint* tex_screen, unsigned int size_x, unsigned int size_y);
void deleteTexture(GLuint* tex);
void createDepthBuffer(GLuint* depth, unsigned int size_x, unsigned int size_y);
void deleteDepthBuffer(GLuint* depth);
void createFramebuffer(GLuint* fbo, GLuint color, GLuint depth);
void deleteFramebuffer(GLuint* fbo);

// rendering callbacks
void display();
void idle();
void keyboard(unsigned char key, int x, int y);
void reshape(int w, int h);
void mainMenu(int i);

///////use to texture geo
static const char *glsl_drawtex_vertshader_src = 
        "void main(void)\n"
        "{\n"
        "gl_Position = gl_Vertex;\n"
        "gl_TexCoord[0].xy = gl_MultiTexCoord0.xy;\n"
        "}\n";


static const char *glsl_drawtex_fragshader_src = 
        "#version 130\n"
        "uniform usampler2D texImage;\n"
        "void main()\n"
        "{\n"
        "   vec4 c = texture(texImage, gl_TexCoord[0].xy);\n"
"gl_FragColor = c / 255.0;\n"
        "}\n";



////////////////////////////////////////////shDrawGLSLShader
static const char * glsl_vel_GLSLvertshader_src =
"#version 130\n"
"out vec3 out_velocity;\n"

"uniform mat4 modelView;\n"
"uniform mat4 prevModelView;\n"
"uniform float blurScale;\n"
"uniform vec3 halfWindowSize;\n"

        "void main(void)\n"
        "{\n"
// transform to eye/camera space

        "vec4 Pprev = prevModelView * gl_Vertex;\n"
        "vec4 P = modelView * gl_Vertex;\n"
      //  "vec3 normal = normalize(gl_NormalMatrix * gl_Normal);\n"////normalize
      //  "vec3 motionVector = P.xyz - Pprev.xyz;\n"

//get the clip space motion vector
"mat4 prevModelViewProj = gl_ProjectionMatrix *prevModelView;\n"//
"P = gl_ModelViewMatrix * gl_Vertex;\n"
/*
"vec4 Pstretch;\n"
"float flag = sign(dot(motionVector, normal));\n"
        "if (flag > 0.0)\n"
        "Pstretch = P;\n"
        "else\n"
"Pstretch=mix(P, Pprev, blurScale);\n" //
*/
"gl_Position = P;\n"//Pstretch;\n"

// calculate window space velocity, do divide by W -> NDC coordinates
"Pprev = prevModelViewProj * gl_Vertex;\n"
"   P.xyz = P.xyz / P.w;\n"
"   Pprev.xyz = Pprev.xyz / Pprev.w;\n"
"   vec3 velocity =halfWindowSize*(P.xyz - Pprev.xyz);\n"

"float len = length (velocity)/(halfWindowSize[0]*2);\n" //[0,1]
"len = clamp(len, 0., 1.);\n"

"velocity =  normalize(velocity);\n"
"velocity.z = len;\n"
"out_velocity =velocity;\n"

 //"gl_TexCoord[0]= gl_MultiTexCoord0;\n"
        "}\n";


static const char *glsl_velGLSL_fragshader_src =
"#version 130\n"
"in vec3 out_velocity;\n"

"uniform sampler2D sceneTex;\n"// beauty
"uniform float samples;\n"
"uniform float blurScale;\n"
"uniform vec3 halfWindowSize;\n"

"void main()\n"
"{\n"
"float w = 1.0 / samples; \n" // weight
"float wsize =halfWindowSize.x*2.; \n"
"float hsize =halfWindowSize.y*2.; \n"
"vec4 a=vec4(0.);\n"
"vec2 velocity =out_velocity.xy *.08* blurScale*out_velocity.z;\n" //blurScale
   // "vec2 velocity =out_velocity.xy*blurScale;\n"
"vec2 scCoord =vec2(gl_FragCoord.x/wsize, gl_FragCoord.y/hsize);\n"

// sample into scene texture along motion vector
"for(float i=0; i<samples; i+=1) \n"
"{ float t = i / (samples-1);\n"
"a =  a +texture2D(sceneTex, scCoord + velocity*t)* w;}\n"
"gl_FragColor =a;\n"

 "}";


/////////////////////shDrawBeauty
static const char *glsl_beauty_vertshader_src =
"#version 130\n"
"out vec3 N;\n"
"out vec3 v;\n"

"uniform mat4 modelView;\n"
"uniform mat4 prevModelView;\n"
"uniform float blurScale;\n"

        "void main(void)\n"
        "{\n"
        "vec4 P = gl_ModelViewMatrix * gl_Vertex;\n"
"gl_Position = P;\n"//Pstretch;\n"

"gl_TexCoord[0].xy = gl_MultiTexCoord0.xy;\n"
"v = P.xyz;  \n"     //Pstretch.xyz
"N = normalize(gl_NormalMatrix * gl_Normal);\n"
        "}\n";



static const char *glsl_beauty_fragshader_src =
"#version 130\n"
"in vec3 N;\n"
"in vec3 v;\n"

"void main()\n"
"{"
   "  vec3 L = normalize(gl_LightSource[0].position.xyz - v);  \n"
   "  vec3 E = normalize(-v); // we are in Eye Coordinates, so EyePos is (0,0,0)  \n"
   "  vec3 R = normalize(-reflect(L,N));\n"


   "  vec4 Iamb = gl_FrontLightProduct[0].ambient; \n"

   "  vec4 Idiff = gl_FrontLightProduct[0].diffuse * max(dot(N,L), 0.0);\n"
   "  Idiff = clamp(Idiff, 0.0, 1.0);\n"

   "  vec4 Ispec = gl_FrontLightProduct[0].specular * pow(max(dot(R,E),0.0),0.3*60);\n"//gl_FrontMaterial.shininess
   "  Ispec = clamp(Ispec, 0.0, 1.0);\n"

   "  gl_FragColor = gl_FrontLightModelProduct.sceneColor + Iamb + Idiff + Ispec;  \n"
//"  gl_FragColor = uvec4(0., 255.0, 0. , 255.0);\n"
"}\n";


////////////////////////////////////////////shDrawVelCUDA
static const char * glsl_vel_CUDAvertshader_src =
"#version 130\n"
"out vec4 out_color;\n"

"uniform mat4 prevModelView;\n"
"uniform vec3 halfWindowSize;\n"

        "void main(void)\n"
        "{\n"
// calculate clip space motion vector
//transform eye space coordinates into clip coordinates gl_ProjectionMatrix
"mat4 prevModelViewProj = gl_ProjectionMatrix *prevModelView;\n"//
"vec4 Pprev = prevModelViewProj * gl_Vertex;\n"
"vec4 P = gl_ModelViewMatrix * gl_Vertex;\n"
"gl_Position = P;\n"

// do divide by W -> NDC coordinates
"   P.xyz = P.xyz / P.w;\n"
"   Pprev.xyz = Pprev.xyz / Pprev.w;\n"
        "   vec3 velocity = (P.xyz - Pprev.xyz) * halfWindowSize;\n"

"float len = length (velocity)/(halfWindowSize[0]*2);\n" //[0,1]
"len = clamp(len, 0., 1.);\n"
"velocity =normalize(velocity);\n"
// to range
"out_color.xy = 0.5 + (velocity.xy* 0.005);\n"  //0.5 + (dP.xy * 0.005);
"out_color.z = len;\n"
        "}\n";

static const char *glsl_velCUDA_fragshader_src=
"#version 130\n"
"in vec4 out_color;\n"

"void main()\n"
"{\n"
//"gl_FragColor =out_color* 255.;\n"//DEBUG
"gl_FragColor =out_color;\n"
//gl_FragCoord.z
 "}";

#endif
