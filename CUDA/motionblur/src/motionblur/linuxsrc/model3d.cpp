
#include <assert.h>
#include <string>

#include "model3d.h"
#include "objLoader.h"
#include "material.h"
#include "GLTools.h"

using namespace std;



static void checkGLErrors(const char *msg="") {
	const GLenum errCode = glGetError();     
	if (errCode != GL_NO_ERROR) 
		fprintf(stderr,"Error (%s) : %s\n",msg,gluErrorString(errCode));
}

Model3D::~Model3D(){
	cleanup();
}

void Model3D::cleanup(){
	if(geometry){
		for(int i=0; i<nm; i++){
			for(int j=0; j<2; j++)
				delete geometry[i][j];
			delete [] geometry[i];
		}
		delete [] geometry;
	}
	if(materials){
		for(int i=0; i<nm; i++)
			delete materials[i];
		delete [] materials;
	}

	bbox=BBox();
	ready=false;
}

void Model3D::render(bool usemat)
{
	int vertsPerFace[2]={3,4};
	//printf("nm: %d\n", nm);

	for(int i=0; i<nm; i++)
	{
		if (usemat)
			materials[i]->activate();

		geometry[i][0]->render();
		geometry[i][1]->render();

		if (usemat)
			materials[i]->deactivate();
	}
}

bool Model3D::intersects(const BBox &bbox) const{
	for(int i=0; i<nm; i++)
		for(int j=0; j<2; j++)
			if(geometry[i][j]->intersects(bbox))
				return true;
	return false;
}

// FIXME: no need to keep loader around after init

void Model3D::loadHeightfield(const char *heightFilename, const char *normalFilename, const char *textureFilename){
	// cleanup any existing data
	cleanup();

	loader=NULL;

	// create single empty material
	nm=1;
	materials=new Material*[nm];
	materials[0]=new Material();

	// construct geometry data
	geometry=new GeometryList**[1];
	geometry[0]=new GeometryList*[2];
	geometry[0][0]=new GeometryList();
	geometry[0][1]=new GeometryList(heightFilename,normalFilename,textureFilename,Vector(.3f,6.f,.3f));

	bbox=geometry[0][1]->getBBox();

	ready=true;
}

void Model3D::loadOBJ(const char *filename)
{
	// cleanup any existing data
	cleanup();

	// load OBJ model data
	loader=new objLoader();
	int ok=loader->load((char*)filename);
	printf("loaded: %d, %s", ok, filename);
	if(!ok)
		return;
	
	// get OBJ file directory
	string fname=filename;
	string path;
	int idx=fname.find_last_of("\\/");
	path=(idx==-1) ? "./" : fname.substr(0,idx)+"/";
	//printf("load idx: %d\n", idx);

	// create material objects, prepending the obj file path to any texture filenames
	nm=loader->materialCount;
	//printf("load nm: %d\n", nm);

	materials=new Material*[nm];
	for(int i=0; i<loader->materialCount; i++){
		string texFname=loader->materialList[i]->texture_filename;
		// trim whitespace from tail
		idx=texFname.find_last_not_of("\n\t");
		if(idx!=string::npos)
			texFname.erase(idx+1);
		// add OBJ file path
		if(texFname.length()>0)
		  texFname=path+texFname;
		sprintf(loader->materialList[i]->texture_filename,texFname.c_str());
		materials[i]=new Material(loader->materialList[i]);	
	}


	// construct geometry data, 1 list per material, per face type (tris, quads)
	geometry=new GeometryList**[nm];
	int vertsPerFace[2]={3,4};
	for(int i=0; i<nm; i++){
		geometry[i]=new GeometryList*[2];
		for(int j=0; j<2; j++){
			geometry[i][j]=new GeometryList(loader,i,vertsPerFace[j]);
			bbox=bbox.Union(geometry[i][j]->getBBox());
		}
	}
		
	ready=true;
}

bool GeometryList::intersects(const BBox &bbox) const{
	if(isHeightfield){
		for(int x=max(0,(int)(bbox.pMin.x/scale.x));x<min(hw,(int)(ceilf(bbox.pMax.x)/scale.x));x++)
			for(int z=max(0,(int)(bbox.pMin.z/scale.z));z<min(hh,(int)(ceilf(bbox.pMax.z)/scale.z));z++)
				// test the first vertex from each face
				// NOTE: this doesn't test verts in the last row/col
				if(bbox.Inside(vertexData[4*((hw-1)*z+x)]))  
					return true;
		return false;
	}else
		return bbox.Overlaps(this->bbox);	
}

GeometryList::GeometryList(const char *heightFilename, const char *normalFilename, 
 	const char *colorFilename, const Vector &scale) : isHeightfield(true),
  materialIndex(0), vertsPerFace(4), numFaces(0), bbox(), hasTexture(false), 
	scale(scale), hasColor(true),texCoordData(NULL){

	useVBOs=glewIsExtensionSupported("GL_ARB_vertex_buffer_object")!=0;

	// read in heightfield and normal maps
	GLint tw,th,tc;
	GLenum tf;
	GLbyte *height=gltReadTGABits(heightFilename,&tw,&th,&tc,&tf);
	assert(tf==GL_LUMINANCE);
	GLbyte *normals=gltReadTGABits(normalFilename,&tw,&th,&tc,&tf);
	GLubyte *color=(GLubyte*)gltReadTGABits(colorFilename,&tw,&th,&tc,&tf);

	hw=(int)tw;
	hh=(int)th;

	// create faces
	numFaces=(tw-1)*(th-1);
	vertexData=new Point[numFaces*vertsPerFace];
	normalData=new Vector[numFaces*vertsPerFace];
	// FIXME: inefficient; no need to store color as float
	colorData=new Vector[numFaces*vertsPerFace];
	int idx=0;
	float dx=1.f/(float)(tw-1);
	float dy=1.f/(float)(th-1);
	int offsets[4][2]={{0,1},{1,1},{1,0},{0,0}};

	// for each face, and vertex within that face,
	for(int y=0; y<th-1; y++){
		for(int x=0; x<tw-1; x++){
			for(int i=0; i<4; i++,idx++){
				// read in vertex, normal, and color data
				int xi=x+offsets[i][0];
				int yi=y+offsets[i][1];
				vertexData[idx]=Point(scale[0]*(float)xi,
					scale[1]*(float)height[yi*tw+xi]/127.f,scale[2]*(float)(yi));
				normalData[idx]=Vector((float)normals[3*(yi*tw+xi)],(float)normals[3*(yi*tw+xi)+1],
					(float)normals[3*(yi*tw+xi)+2]).Hat();
				colorData[idx]=Vector((((float)color[3*(yi*tw+xi)+2]))/255.f,((float)color[3*(yi*tw+xi)+1])/255.f,
					((float)color[3*(yi*tw+xi)])/255.f);
				// update the bounding box with the new vertex
				bbox=bbox.Union(vertexData[idx]);  
			}
		}
	}
#ifdef COMPUTE_TERRAIN_NORMALS
	// compute face normals 
	// FIXME: no need to duplicate each face normal 4x
	idx=0;
	Vector *faceNormals=new Vector[numFaces];
	for(int y=0; y<th-1; y++)
		for(int x=0; x<tw-1; x++,idx++)
			faceNormals[idx]=Cross(vertexData[4*idx+1]-vertexData[4*idx],vertexData[4*idx+2]-vertexData[4*idx]).Hat();
		
	// compute vertex normals
	idx=0;
	//Vector *computedNormals=new Vector[numFaces*vertsPerFace];
	for(int y=0; y<th-1; y++){
		for(int x=0; x<tw-1; x++){
			for(int i=0; i<4; i++,idx++){
				int xi=x+offsets[i][0];
				int yi=y+offsets[i][1];
				
				int nfi=0;
				normalData[idx]=Vector(0.f,0.f,0.f);
				if(xi>0 && yi<th-2){
					normalData[idx]=normalData[idx]+faceNormals[yi*(tw-1)+xi-1];
					nfi++;
				}
				if(xi>0 && yi>0){
					normalData[idx]=normalData[idx]+faceNormals[(yi-1)*(tw-1)+xi-1];
					nfi++;
				}
				if(xi<tw-2 && yi>0){
					normalData[idx]=normalData[idx]+faceNormals[(yi-1)*(tw-1)+xi];
					nfi++;
				}
				if(xi<tw-2 && yi<th-2){
					normalData[idx]=normalData[idx]+faceNormals[yi*(tw-1)+xi];
					nfi++;
				}
				assert(nfi>0);
				normalData[idx]=normalData[idx].Hat();
			}
		}
	}

	delete [] faceNormals;
#endif

	// cleanup TGA data
	free(normals);
	free(height);
	free(color);

	// initialize VBOs
	if(useVBOs)
		setupVBOs();

}

GeometryList::GeometryList() :  materialIndex(-1), vertsPerFace(0), numFaces(0), bbox(),
	hasTexture(false),hasColor(false), isHeightfield(false),vertexData(NULL),texCoordData(NULL),
  normalData(NULL),colorData(NULL){
	useVBOs=glewIsExtensionSupported("GL_ARB_vertex_buffer_object")!=0;
}


GeometryList::GeometryList(objLoader *loader, int materialIndex, int vertsPerFace) :
  materialIndex(materialIndex), vertsPerFace(vertsPerFace), bbox(), numFaces(0),hasTexture(false),
	hasColor(false),isHeightfield(false)
{

	useVBOs=glewIsExtensionSupported("GL_ARB_vertex_buffer_object")!=0;

	assert(vertsPerFace==3 || vertsPerFace==4);
	hasTexture=strlen(loader->materialList[materialIndex]->texture_filename)>0; 

	// count faces
	for(int i=0; i<loader->faceCount; i++){
		obj_face *face=loader->faceList[i];	
		if(face->material_index==materialIndex && face->vertex_count==vertsPerFace)
			numFaces++;
	}

	// allocate space
	vertexData=new Point[numFaces*vertsPerFace];
	normalData=new Vector[numFaces*vertsPerFace];
	texCoordData=(hasTexture) ? new Vector[numFaces*vertsPerFace] : NULL;
	// store, duplicating as necessary
	int j=0;
	for(int i=0; i<loader->faceCount; i++){
		obj_face *face=loader->faceList[i];	
		if(face->material_index==materialIndex && face->vertex_count==vertsPerFace){
			for(int k=0; k<vertsPerFace; k++,j++){
				for(int c=0; c<3; c++){
					vertexData[j][c]=(float)loader->vertexList[face->vertex_index[k]]->e[c];
					normalData[j][c]=(float)loader->normalList[face->normal_index[k]]->e[c];
					if(hasTexture)
						texCoordData[j][c]=min(1.f,max(0.f,(float)loader->textureList[face->texture_index[k]]->e[c]));
				}
				bbox=bbox.Union(vertexData[j]);
			}
		}
	}

	if(useVBOs)
		setupVBOs();
}

void GeometryList::setupVBOs(){
	glGenBuffersARB(1,&vertexVBO);
	glBindBufferARB(GL_ARRAY_BUFFER,vertexVBO);
	glBufferDataARB(GL_ARRAY_BUFFER,numFaces*vertsPerFace*3*sizeof(float),
		vertexData,GL_STATIC_DRAW_ARB);

	glGenBuffersARB(1,&normalVBO);
	glBindBufferARB(GL_ARRAY_BUFFER,normalVBO);
	glBufferDataARB(GL_ARRAY_BUFFER,numFaces*vertsPerFace*3*sizeof(float),
		normalData,GL_STATIC_DRAW_ARB);

	if(hasColor){
		glGenBuffersARB(1,&colorVBO);
		glBindBufferARB(GL_ARRAY_BUFFER,colorVBO);
		glBufferDataARB(GL_ARRAY_BUFFER,numFaces*vertsPerFace*3*sizeof(float),
			colorData,GL_STATIC_DRAW_ARB);
	}

	if(hasTexture){
		glGenBuffersARB(1,&texCoordVBO);
		glBindBufferARB(GL_ARRAY_BUFFER,texCoordVBO);
		glBufferDataARB(GL_ARRAY_BUFFER,numFaces*vertsPerFace*3*sizeof(float),
			texCoordData,GL_STATIC_DRAW_ARB);
	}
}

void GeometryList::teardownVBOs()
{
	// FIXME: writeme
}


void GeometryList::render()
{
	GLenum faceType=(vertsPerFace==3) ? GL_TRIANGLES : GL_QUADS;
	//printf("hasColor: %d, %d, %d", hasColor, hasTexture, useVBOs);

	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_NORMAL_ARRAY);
	if(hasTexture){
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		if(useVBOs){
			glBindBufferARB(GL_ARRAY_BUFFER_ARB,texCoordVBO);
			glTexCoordPointer(3,GL_FLOAT,0,NULL);
		}else
			glTexCoordPointer(3,GL_FLOAT,0,texCoordData);
	}
	if(hasColor){
		glColorMaterial(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE);
		glEnable(GL_COLOR_MATERIAL);
		glShadeModel(GL_SMOOTH);
		glEnableClientState(GL_COLOR_ARRAY);
		if(useVBOs){
			glBindBufferARB(GL_ARRAY_BUFFER_ARB,colorVBO);
			glColorPointer(3,GL_FLOAT,0,NULL);
		}else
			glColorPointer(3,GL_FLOAT,0,colorData);
	}

	if(useVBOs){
		glBindBufferARB(GL_ARRAY_BUFFER_ARB,normalVBO);
		glNormalPointer(GL_FLOAT,0,NULL);
		glBindBufferARB(GL_ARRAY_BUFFER_ARB,vertexVBO);
		glVertexPointer(3,GL_FLOAT,0,NULL);
	}else{
		glNormalPointer(GL_FLOAT,0,normalData);
		glVertexPointer(3,GL_FLOAT,0,vertexData);
	}
	
	glDrawArrays(faceType,0,numFaces*vertsPerFace);

	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_COLOR_ARRAY);
	glDisable(GL_COLOR_MATERIAL);
}

GeometryList::~GeometryList(){
	delete [] vertexData;
	delete [] texCoordData;
	delete [] normalData;

	teardownVBOs();

}
