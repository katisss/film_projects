 
 // Utilities and system includes
#include <cutil_inline.h>


texture<float4, 2, cudaReadModeElementType> inTex;
texture<float4, 2, cudaReadModeElementType> velocityTex1;//////

cudaArray *g_last_array;

// clamp x to range [a, b]
__device__ float clamp(float x, float a, float b)
{
    return max(a, min(b, x));
}

__device__ int clamp(int x, int a, int b)
{
    return max(a, min(b, x));
}

// convert floating point rgb color to 8-bit integer
__device__ int rgbToInt(float r, float g, float b)
{
    return (int(b)<<16) | (int(g)<<8) | int(r);
}

///////////
__constant__ float sampleWeight[256];
extern void setParam(float numSamples) 
{
	float some_data[256];
    for (int i = 0; i < sizeof(some_data) / sizeof(some_data[0]); i++)
    {  
		some_data[i] =float(i)/(numSamples-1.);
	//	printf("some_data[i]: %.2f", some_data[i]);
    }
    some_data[255]=255.0f/numSamples;
    cudaMemcpyToSymbol("sampleWeight", some_data, 256 * sizeof(float), 0);
}


//#define SMEM(X, Y) sdata[(Y)*tilew+(X)]
	
__device__ float4 getPixel(int x, int y)
{
    return tex2D(inTex, x, y);
}
 
///////////////////////////////////////////////////////////////////////////////
__global__ void cudaMBlur(unsigned int* g_odata, int imgw, int imgh, 
	   float blurScale, float numSamples)
{
    int x = __mul24(blockIdx.x, blockDim.x) + threadIdx.x;
    int y = __mul24(blockIdx.y, blockDim.y) + threadIdx.y;
/*
    extern __shared__ float4 sdata[];

    int tx = threadIdx.x;
    int ty = threadIdx.y;
    int bw = blockDim.x;
    int bh = blockDim.y;
	int r=23;//max

    // copy tile to shared memory
    // center region
    SMEM(r + tx, r + ty) = getPixel(x, y);
    // borders
    if (threadIdx.x < r) 
    {
        // left
        SMEM(tx, r + ty) = getPixel(x - r, y);
        // right
        SMEM(r + bw + tx, r + ty) = getPixel(x + bw, y);
    }
    if (threadIdx.y < r) 
    {
        // top
        SMEM(r + tx, ty) = getPixel(x, y - r);
        // bottom
        SMEM(r + tx, r + bh + ty) = getPixel(x, y+ bh);
    }

    // load corners
    if ((threadIdx.x < r) && (threadIdx.y < r)) 
    {
        // tl
        SMEM(tx, ty) = getPixel(x - r, y - r);
        // bl
        SMEM(tx, r + bh + ty) = getPixel(x - r, y + bh);
        // tr
        SMEM(r + bw + tx, ty) = getPixel(x + bh, y - r);
        // br
        SMEM(r + bw + tx, r + bh + ty) = getPixel(x + bw, y + bh);
    }

    // wait for loads to complete
    __syncthreads();
	*/
	
    if (x < imgw && y < imgh) 
    {
		float4 velocityT=tex2D(velocityTex1, x, y);// current velocity
		float l= __fmul_rn(blurScale,velocityT.z);
		velocityT=make_float4(__fmul_rn(velocityT.x, l), __fmul_rn(velocityT.y, l), 1.,1.);
		
		float4 f4;
		float4 currentFColor =make_float4(0.,0.,0.,0.); 
		if (velocityT.z>.09 )
		{
			for(float i = 1; i < numSamples; ++i)  
			{  
				float4 blurColor =tex2D(inTex, (x + __fmul_rn(velocityT.x, sampleWeight[int(i)]) ), 
					(y + __fmul_rn(velocityT.y, sampleWeight[int(i)]) ) );	
				// Add the current color to our color sum 
				currentFColor = make_float4(currentFColor.x+blurColor.x, currentFColor.y+blurColor.y,
						currentFColor.z+blurColor.z, 1.); 	
			}
		 // Average all of the samples to get the final blur color 
		  f4= make_float4(__fmul_rn(currentFColor.x,sampleWeight[255]), __fmul_rn(currentFColor.y,sampleWeight[255]),	
			__fmul_rn(currentFColor.z,sampleWeight[255]), 1.);
		}
		else
		{
			currentFColor =tex2D(inTex, x, y);
			 f4=make_float4(__fmul_rn(currentFColor.x,255.), __fmul_rn(currentFColor.y,255.),	
			__fmul_rn(currentFColor.z,255.), 1.);
		}

 		 g_odata[__mul24(y,imgw)+x] = rgbToInt(f4.x, f4.y, f4.z);	
 	 }
}



///////////////main
extern "C" void launch_cudaProcess(dim3 grid, dim3 block, int sbytes, cudaArray *g_Ddata_array,
		   cudaArray *g_data_array, unsigned int* g_odata, 
		   int imgw, int imgh, int tilew, float blurScale, float numSamples)
{
    cutilSafeCall(cudaBindTextureToArray(inTex, g_data_array));
    cutilSafeCall(cudaBindTextureToArray(velocityTex1, g_Ddata_array));
    cudaMBlur<<< grid, block, sbytes >>> (g_odata, imgw, imgh, blurScale, numSamples);  
          
	//cudaMBlur<<< grid, block, sbytes >>> (g_odata, imgw, imgh, blurScale, numSamples);   	
	//cutilSafeCall(cudaThreadSynchronize());
}
	    





