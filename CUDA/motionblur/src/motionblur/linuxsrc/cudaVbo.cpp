
/* 
    This example demonstrates how to use the Cuda OpenGL bindings to
    dynamically modify a vertex buffer using a Cuda kernel.

    The steps are:
    1. Create an empty vertex buffer object (VBO)
    2. Register the VBO with Cuda
    3. Map the VBO for writing from Cuda
    4. Run Cuda kernel to modify the vertex positions
    5. Unmap the VBO
    6. Render the results using OpenGL

    Host code
*/

#ifdef _WIN32
#  define WINDOWS_LEAN_AND_MEAN
#  define NOMINMAX
#  include <windows.h>
#endif

// includes, system
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <iostream>

// includes, GL
#include <GL/glew.h>

#if defined (__APPLE__) || defined(MACOSX)
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

// includes
#include <cuda_runtime.h>
#include <cutil_inline.h>
#include <cutil_gl_inline.h>

#include <cuda_gl_interop.h>

#include <vector_types.h>
#include <rendercheck_gl.h>
#include "blur.h"

#include <shared/data_path.h>
#include <shared/glh/glh_linear.h>//matix stuff

////////////////////////////////////////////////////////////////////////////////
// constants
float anim = 0.0;

// mouse controls
int mouse_old_x, mouse_old_y;
int mouse_buttons = 0;

float rotate_x = 0.0, rotate_y = 0.0;
float translate_z = -3.0;

unsigned int ctimer = 0;

// Auto-Verification Code
const int frameCheckNumber = 4;
int fpsCount = 0;        // FPS count for averaging
int fpsLimit = 1;        // FPS limit for sampling
int gIndex = 0;
unsigned int frameCount = 0;
//unsigned int gTotalErrors = 0;

// CheckFBO/BackBuffer class objects
CheckRender       *gCheckRender = NULL;

#define MAX(a,b) ((a > b) ? a : b)


// cube ///////////////////////////////////////////////////////////////////////
//    v6----- v5
//   /|      /|
//  v1------v0|
//  | |     | |
//  | |v7---|-|v4
//  |/      |/
//  v2------v3

// vertex coords array
GLfloat vertices[] = {.15,.15,.15,  -.15,.15,.15,  -.15,-.15,.15,  .15,-.15,.15,        // v0-v1-v2-v3
                      .15,.15,.15,  .15,-.15,.15,  .15,-.15,-.15,  .15,.15,-.15,        // v0-v3-v4-v5
                      .15,.15,.15,  .15,.15,-.15,  -.15,.15,-.15,  -.15,.15,.15,        // v0-v5-v6-v1
                      -.15,.15,.15,  -.15,.15,-.15,  -.15,-.15,-.15,  -.15,-.15,.15,    // v1-v6-v7-v2
                      -.15,-.15,-.15,  .15,-.15,-.15,  .15,-.15,.15,  -.15,-.15,.15,    // v7-v4-v3-v2
                      .15,-.15,-.15,  -.15,-.15,-.15,  -.15,.15,-.15,  .15,.15,-.15};   // v4-v7-v6-v5

// normal array
GLfloat normals[] = {0,0,1,  0,0,1,  0,0,1,  0,0,1,             // v0-v1-v2-v3
                     1,0,0,  1,0,0,  1,0,0, 1,0,0,              // v0-v3-v4-v5
                     0,1,0,  0,1,0,  0,1,0, 0,1,0,              // v0-v5-v6-v1
                     -1,0,0,  -1,0,0, -1,0,0,  -1,0,0,          // v1-v6-v7-v2
                     0,-1,0,  0,-1,0,  0,-1,0,  0,-1,0,         // v7-v4-v3-v2
                     0,0,-1,  0,0,-1,  0,0,-1,  0,0,-1};        // v4-v7-v6-v5

unsigned int vertSize=sizeof(vertices);//+sizeof(normals);

extern "C" 
void launch_VetexKernel(float4* pos, unsigned int mesh_width, unsigned int mesh_height, float time);

////////////////////////////////////////////////////////////////////////////////
// declaration, forward
//CUTBoolean runTest(int argc, char** argv);
void cleanup();
void motion(int x, int y);



void copyWindowToTexture(GLenum target, GLuint *tex)
{  
  if (*tex) {
    // copy to texture
    glBindTexture(target, *tex);
    glCopyTexSubImage2D(target, 0, 0, 0, 0, 0, window_width, window_height);
  } else {
    // create texture
    glGenTextures(1, tex);
    glBindTexture(target, *tex);

    glTexParameteri(target, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(target, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(target, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(target, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    glCopyTexImage2D(target, 0, GL_RGB, 0, 0, window_width, window_height, 0);
  }
}

int keydown[256];
bool toggle[256];
glh::matrix4f projectionCG, modelViewCG, prevModelViewCG;

void cleanExit(int exitval)
{
	/*
  if (mblur_vprog) cgDestroyProgram(mblur_vprog);
  if (mblur_fprog) cgDestroyProgram(mblur_fprog);
  if (context) cgDestroyContext(context);
*/
  if(exitval == 0) { exit(0); }
//  else { quitapp(exitval); }
}

void AutoQATest()
{
    if (gCheckRender && gCheckRender->IsQAReadback()) {
        char temp[256];
        sprintf(temp, "AutoTest: Cuda GL Interop (VBO)");
	glutSetWindowTitle(temp);
	exit(0);
    }
}

void computeFPS()
{
    frameCount++;
    fpsCount++;

    if (fpsCount == fpsLimit) {
        char fps[256];
        float ifps = 1.f / (cutGetAverageTimerValue(ctimer) / 1000.f);
        sprintf(fps, "%sCuda GL Interop (VBO): %3.1f fps", 
                ((gCheckRender && gCheckRender->IsQAReadback()) ? "AutoTest: " : ""), ifps);  

        glutSetWindowTitle(fps);
        fpsCount = 0; 
        if (gCheckRender && !gCheckRender->IsQAReadback()) 
	    fpsLimit = (int)MAX(ifps, 1.f);

        cutilCheckError(cutResetTimer(ctimer));  

        AutoQATest();
    }
 }



void runCuda(struct cudaGraphicsResource **vbo_resource)
{
    // map OpenGL buffer object for writing from CUDA
    float4 *dptr;
    // DEPRECATED: cutilSafeCall(cudaGLMapBufferObject((void**)&dptr, vboID));
    cutilSafeCall(cudaGraphicsMapResources(1, vbo_resource, 0));
    size_t num_bytes; 
    cutilSafeCall(cudaGraphicsResourceGetMappedPointer((void **)&dptr, &num_bytes,  
						       *vbo_resource));

    //printf("CUDA mapped VBO: May access %ld bytes\n", num_bytes);
	cudaArray *vel_array;
	//launch_VetexKernel(dptr, 4, 6, anim);
	anim += 0.01;
	/*
    // execute the kernel
    //    dim3 block(8, 8, 1);
    //    dim3 grid(mesh_width / block.x, mesh_height / block.y, 1);
    //    kernel<<< grid, block>>>(dptr, mesh_width, mesh_height, anim);

    launch_VetexKernel(dptr, mesh_width, mesh_height, anim);
	*/
    // unmap buffer object
    // DEPRECATED: cutilSafeCall(cudaGLUnmapBufferObject(vboID));

    cutilSafeCall(cudaGraphicsUnmapResources(1, vbo_resource, 0));
}

////////////////////////////////////////////////////////////////////////////////
void createVBO(GLuint* vboID, struct cudaGraphicsResource **vbo_res, 
	       unsigned int vbo_res_flags)
{
    if (vboID) 
	{
	// create buffer object
	glGenBuffers(1, vboID);
	glBindBuffer(GL_ARRAY_BUFFER, *vboID);

	// initialize buffer object
//	unsigned int size = mesh_width * mesh_height * 4 * sizeof(float);
	glBufferDataARB(GL_ARRAY_BUFFER_ARB, sizeof(vertices)+sizeof(normals), 0, GL_STATIC_DRAW_ARB);
    glBufferSubDataARB(GL_ARRAY_BUFFER_ARB, 0, sizeof(vertices), vertices);                             // copy vertices starting from 0 offest
    glBufferSubDataARB(GL_ARRAY_BUFFER_ARB, sizeof(vertices), sizeof(normals), normals);  
	//glBufferData(GL_ARRAY_BUFFER, size, 0, GL_DYNAMIC_DRAW);
	/*
	    glGenBuffersARB(1, &vboId);
        glBindBufferARB(GL_ARRAY_BUFFER_ARB, vboId);
        glBufferDataARB(GL_ARRAY_BUFFER_ARB, sizeof(vertices)+sizeof(normals)+sizeof(colors), 0, GL_STATIC_DRAW_ARB);
        glBufferSubDataARB(GL_ARRAY_BUFFER_ARB, 0, sizeof(vertices), vertices);                             // copy vertices starting from 0 offest
        glBufferSubDataARB(GL_ARRAY_BUFFER_ARB, sizeof(vertices), sizeof(normals), normals);                // copy normals after vertices
        glBufferSubDataARB(GL_ARRAY_BUFFER_ARB, sizeof(vertices)+sizeof(normals), sizeof(colors), colors);  // copy colours after normals
  
  */
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	// register this buffer object with CUDA
	// DEPRECATED: cutilSafeCall(cudaGLRegisterBufferObject(*vboID));
	cutilSafeCall(cudaGraphicsGLRegisterBuffer(vbo_res, *vboID, vbo_res_flags));
	
	//CUT_CHECK_ERROR_GL();
    } else {
	cutilSafeCall( cudaMalloc( (void **)&d_vbo_buffer, sizeof(vertices)*4*sizeof(float) ) );
    }
}

////////////////////////////////////////////////////////////////////////////////
//! Delete VBO
////////////////////////////////////////////////////////////////////////////////
void deleteVBO(GLuint* vboID, struct cudaGraphicsResource *vbo_res)
{
    if (vboID) {
	// unregister this buffer object with CUDA
	//DEPRECATED: cutilSafeCall(cudaGLUnregisterBufferObject(*pbo));
	cudaGraphicsUnregisterResource(vbo_res);
	
	glBindBuffer(1, *vboID);
	glDeleteBuffers(1, vboID);
	
	*vboID = 0;
    } else {
	cudaFree(d_vbo_buffer);
	d_vbo_buffer = NULL;
    }
}



void cleanup()
{
    cutilCheckError( cutDeleteTimer( ctimer));

    deleteVBO(&vboID, cudaVbo_resource);

    if (gCheckRender) {
        delete gCheckRender; gCheckRender = NULL;
    }
}


void motion(int x, int y)
{
    float dx, dy;

    dx = x - mouse_old_x;
    dy = y - mouse_old_y;

    if (mouse_buttons & 1) {
        rotate_x += dy * 0.2;
        rotate_y += dx * 0.2;
    } else if (mouse_buttons & 4) {
        translate_z += dy * 0.01;
    }

    mouse_old_x = x;
    mouse_old_y = y;
}

////////////////////////////////////////////////////////////////////////////////
//! Check if the result is correct or write data to file for external
//! regression testing
////////////////////////////////////////////////////////////////////////////////
void check_ResultCuda(int argc, char** argv, const GLuint& vboID)
{
    if (!d_vbo_buffer) {
	//DEPRECATED: cutilSafeCall(cudaGLUnregisterBufferObject(vboID));
	cudaGraphicsUnregisterResource(cudaVbo_resource);
	
	// map buffer object
	glBindBuffer(GL_ARRAY_BUFFER_ARB, vboID );
	float* data = (float*) glMapBuffer(GL_ARRAY_BUFFER, GL_READ_ONLY);
	
	// check result
	/*
	if(cutCheckCmdLineFlag(argc, (const char**) argv, "regression")) {
	    // write file for regression test
	    cutilCheckError(cutWriteFilef("./data/regression.dat",
					  data, mesh_width * mesh_height * 3, 0.0));
	}
	*/
	// unmap GL buffer object
	if(! glUnmapBuffer(GL_ARRAY_BUFFER)) {
	    fprintf(stderr, "Unmap buffer failed.\n");
	    fflush(stderr);
	}
	
	//DEPRECATED: cutilSafeCall(cudaGLRegisterBufferObject(vboID));
	cutilSafeCall(cudaGraphicsGLRegisterBuffer(&cudaVbo_resource, vboID, 
						   cudaGraphicsMapFlagsWriteDiscard));
	
	//CUT_CHECK_ERROR_GL();
    }
}

