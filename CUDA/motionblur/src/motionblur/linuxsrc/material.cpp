
#define GLEW_STATIC
#include <GL/glew.h>
#ifdef __MAC__
#	include <GLUT/glut.h>
#else
#  define FREEGLUT_STATIC
#  include <GL/glut.h>
#endif

#include <string>

#include "GLTools.h"

#include "shaders.h"
#include "material.h"
#include "obj_parser.h"

using namespace std;

static void checkGLErrors(const char *msg="") {
	const GLenum errCode = glGetError();      // check for errors
	if (errCode != GL_NO_ERROR) 
		fprintf(stderr,"Error (%s) : %s\n",msg,gluErrorString(errCode));
}

Material::Material(obj_material *objMaterial) : hasTex(false){
	// grab phong parameters
	// HACK: adding to ambient, diffuse, and specular since we're ignoring
	//       the additional texture maps
	for(int i=0; i<3; i++){
		ambient[i]=(float)objMaterial->amb[i]+0.2f;
		diffuse[i]=(float)objMaterial->diff[i]+0.2f;
		specular[i]=(float)objMaterial->spec[i]+0.2f;
	}
	specPower=(float)objMaterial->shiny;
	trans=(float)objMaterial->trans;

	// read texture image and load texture onto GPU
	string fname=objMaterial->texture_filename;
	if(fname.length()>0){
		hasTex=true;
		// read data from disk
		string ext=fname.substr(fname.find_last_of(".")+1);
		GLbyte *texData;
		if(ext.compare(0,3,"tga")==0)
			texData=gltReadTGABits(fname.c_str(),
			  &texWidth,&texHeight,&texComp,&texFormat);
		// NOTE: gltReadBMPBits() doesn't seem to work as advertised...
		//else if(ext.compare(0,3,"bmp")==0)
		//	texData=gltReadBMPBits(fname.c_str(),&texWidth,&texHeight);
		else
			printf("Material::Material(): texture extension not supported\n");
		if(!texData)
			fprintf(stderr,"Material::Material(): failed to load texture %s\n",fname.c_str());
		// setup texture on GPU
		glGenTextures(1,&texHandle);
		glBindTexture(GL_TEXTURE_2D,texHandle);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_REPEAT);
		// read texture data onto GPU and generate mipmap
		glTexImage2D(GL_TEXTURE_2D,0,texComp,texWidth,texHeight,0,texFormat,GL_UNSIGNED_BYTE,texData);
		glGenerateMipmap( GL_TEXTURE_2D );
		//free(texData);
	}
}

Material::Material(){
	// set default phong parameters
	for(int i=0; i<3; i++){
		ambient[i]=0.f;
		diffuse[i]=0.f;
		specular[i]=0.f;
	}
	specPower=0.f;
	trans=1.f;

	hasTex=false;
}

Material::Material(const char *textureFilename){

	// set default phong parameters
	for(int i=0; i<3; i++){
		ambient[i]=0.f;
		diffuse[i]=0.f;
		specular[i]=0.f;
	}
	specPower=0.f;
	trans=1.f;

	string fname=textureFilename;
	hasTex=true;
	// read data from disk
	string ext=fname.substr(fname.find_last_of(".")+1);
	GLbyte *texData;
	if(ext.compare(0,3,"tga")==0)
		texData=gltReadTGABits(fname.c_str(),
		&texWidth,&texHeight,&texComp,&texFormat);
	// NOTE: gltReadBMPBits() doesn't seem to work as advertised...
	//else if(ext.compare(0,3,"bmp")==0)
	//	texData=gltReadBMPBits(fname.c_str(),&texWidth,&texHeight);
	else
		printf("Material: texture extension not supported\n");
	if(!texData)
		fprintf(stderr,"Material::Material(): failed to load texture %s\n",fname.c_str());
	// setup texture on GPU
	glGenTextures(1,&texHandle);
	glBindTexture(GL_TEXTURE_2D,texHandle);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_REPEAT);
	// read texture data onto GPU and generate mipmaps
	glTexImage2D(GL_TEXTURE_2D,0,texComp,texWidth,texHeight,0,texFormat,GL_UNSIGNED_BYTE,texData);
	glGenerateMipmap( GL_TEXTURE_2D );
	//free(texData);

}

Material::~Material(){
	if(hasTex){
		// fixme: release texture memory on GPU
	}
}

void Material::activate(){
	glUseProgram(0);

	if(hasTex){
		glActiveTexture(MATERIAL_TEXTURE_UNIT);
		glClientActiveTexture(MATERIAL_TEXTURE_UNIT);
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D,texHandle);
		glTexEnvi(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_MODULATE);
	}else
		glDisable(GL_TEXTURE_2D);

	// setup material colors
	glPushAttrib(GL_LIGHTING_BIT);
	glLightModeli(GL_LIGHT_MODEL_COLOR_CONTROL,GL_SEPARATE_SPECULAR_COLOR);
	glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT,ambient);
	glMaterialfv(GL_FRONT_AND_BACK,GL_DIFFUSE,diffuse);
	glMaterialfv(GL_FRONT_AND_BACK,GL_SPECULAR,specular);
	glMaterialf(GL_FRONT_AND_BACK,GL_SHININESS,specPower);
		
	// setup blending
	glPushAttrib(GL_ENABLE_BIT);
	if(trans<1){
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	}else
		glDisable(GL_BLEND);
}

void Material::deactivate(){
	glPopAttrib();
	glPopAttrib();
	glUseProgram(0);
	glDisable(GL_TEXTURE_2D);
}