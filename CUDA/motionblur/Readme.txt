Executable:
motionblur
build to bin directory

Sourcefiles:
-in src:
motionBlur.cu
main.cpp
cudaVbo.cpp
blur.h

-objLoader source files in src/motionblur/objLoader:
material.cpp
model.cpp
objLoader.cpp
obj_parser.cpp
shaders.cpp
glInfo.cpp
list.cpp
string_extra.cpp

Uses the following libraries:
-cudart, cutil, shrUtils, glew32, glut
-GLTools (for the objLoader) is included in souce code 
in src/motionblur/Libraries/GLTools 

To run:
In order for the ship model to be loaded correctly from a relative path it must be in the same directory as the executable program while the .mtl must be in the source dir (we already noticed some bugs in CS175 but I didnt want to spend time on this).

A right mouse menu in the window shows all the user settable 
options which are:
- Enable CUDA or GLSL [space bar]
- animate [a]
- Toggle cube/ship [c]
- Render only [r]
- Velocity only [v]
- Decrease number of samples [n]
- Increase number of samples [m]
- Increase blur scale [+]
- Decrease blur scale [-]
