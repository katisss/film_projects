# Sources 
main.cpp
shaders.cpp (from course home work)
particle.cpp

particle.h 
force.h
collision.h (from course  home work)
Vector.h

# Libraries used:
-gltools.lib
-glew32.lib
-freeglut_static.lib

# How to install and run the program
Run the release config of fboParticles.sln, the executable is created in bin folder.

# A brief users guide, i.e., what do the keys/mouse buttons do, etc.
Start the executable and right mouse click to see the options in the menu.
The following options are supported:
-Cycle selected emitter [w]
-Toggle selected emitter on off [x]", 'x');
-Animate selected emitter [a]", 'a');
-Double emission rates [p]
-Half emission rates [t]
-Double new particles life times [s]
-Half new particles life times [q]

-Reflect global force direction [m]
-Increase global force strength [n]
-Decrease global force strength [l]
-Toggle groundCollision [o]

-Display debug cube [e]
-Toogle position texture 1, 2 [b]
-Display attribute texture [c]

-Reset emitter [r]


Known problems:
Coordinates in the float textures arent normalized to positive space

# online resources that helped you during the development of the code
-http://wiki.delphigl.com/index.php/Tutorial_Framebufferobject (german)
-http://wiki.delphigl.com/index.php/GLSL_Partikel (german)
-http://www.songho.ca/opengl/gl_fbo.html 
-"mega particles" at www.2ld.de/gdc2004