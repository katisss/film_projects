#extension GL_EXT_gpu_shader4 : enable
#extension GL_EXT_geometry_shader4: enable
 
uniform samplerBuffer tboVelocity;
uniform usamplerBuffer tboRandom;
uniform float timeElapsed;//////
 
varying out vec4 velocity_out;
varying out unsigned int seed;
 
const vec3 wind = vec3(1.0, 1.0, 2.0);
 
vec3 constField() 
{
   return timeElapsed * wind;
}
 
vec3 dampField(vec3 velocity, float strength) 
{
   return -velocity*strength*timeElapsed;
}
 
vec3 noiseField(float strength) 
{
   vec3 result;
   seed = (seed * 1103515245u + 12345u); result.x = float(seed);
   seed = (seed * 1103515245u + 12345u); result.y = float(seed);
   seed = (seed * 1103515245u + 12345u); result.z = float(seed);
   return timeElapsed * strength * ((result / 4294967296.0) - vec3(0.5,0.5,0.5));
}
 
void main() 
{
   // get all particle data
   vec3 position = gl_PositionIn[0].xyz;
   float size = gl_PositionIn[0].w;
 
   vec4 tmp = texelFetchBuffer(tboVelocity, gl_PrimitiveIDIn);
   vec3 velocity = tmp.xyz;
   float lifetime = tmp.w;
 
   timeElapsed=.001;
   // update lifetime and discard dead particles
   lifetime -= timeElapsed;
   if (lifetime <= 0.0) 
      return; 
 
   // update position and size
   position += velocity * timeElapsed;
   size += 0.3 * size * timeElapsed;
 
   // read current random seed
   seed = texelFetchBuffer(tboRandom, gl_PrimitiveIDIn).x;
 
   // update velocity by applying force fields
   velocity += dampField(velocity, 0.5);
   velocity += constField();
   velocity += noiseField(1.0);
 
   // write vertex
   gl_Position = vec4(position, size);
   velocity_out = vec4(velocity, lifetime);
   EmitVertex();
}