uniform vec3 fvEyePosition;

varying vec3 ViewDirection;
varying vec3 Normal;
varying vec2 fvVaryingTexCoords;
   
void main( void )
{
   gl_Position = ftransform();
    
   vec4 fvObjectPosition = gl_ModelViewMatrix * gl_Vertex;
   fvVaryingTexCoords = gl_MultiTexCoord0.st; 
   ViewDirection  = fvEyePosition - fvObjectPosition.xyz;
   Normal         = gl_NormalMatrix * gl_Normal;
}