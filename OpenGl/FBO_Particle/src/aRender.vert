#version 120
/*
gl.glEnable(GL.GL_POINT_SPRITE);
gl.glEnable(gl.GL_VERTEX_PROGRAM_POINT_SIZE);
*/
//Read position from texture
uniform sampler2D Texture0;
uniform sampler2D attrMap; //static attrs


void main(void)
{
   vec4 look = texture2D(Texture0, vec2(gl_Vertex));
   look.w = 1.0; 

   gl_Position = gl_ModelViewProjectionMatrix * look;
   gl_TexCoord[0] = gl_MultiTexCoord0;
}