﻿#ifndef force_H
#define force_H


#include <vector>
#include "Vector.h"
/*
Update velocity PosTextures with various operations:
– Global forcesare position-invariant:Gravity, wind
- Local forces fall off based on distance: Magnet, orbiting, turbulence, vortex
– Dampening
– Collisions
– Flow field
Fall off with or at hard boundary
Individual m_particles might scale effect based on mass, air resistance etc.
1/ r*r*epsilon 
r: distance, small epsilon
Add all forces to one force vector 
Convert force to acceleration (F=m*a )
– identical if all m_particles have unit mass

global forces (e.g. gravity, wind), local forces (attraction, repulsion), velocity
dampening, and collision responses. For our GPU-based particle system these operations
need to be parameterized via pixel shader constants. Their dynamic combination is a typical
problem of real-time graphics. It is comparable to the problem of light sources and material
combinations and can be solved in similar ways. Typical operation combinations are to be
prepared in several variations beforehand. Other operations can be applied in separate
passes, as all operations are completely independent.

Global forces, e.g. gravity, influence m_particles regardless of their position with a constant
acceleration in a specific direction. The influence of local forces however depends on the
particle's position which is read from the position texture. A magnet attracting or repelling a
particle has a local acceleration towards a point. This force can fall off with the inverse square
of the distance or it can stay constant up to a maximum distance . 
A particle can also be accelerated towards its closest point on a line, leading to a vortex-like
streaming.
A more complex local force can be extracted from a flow field texture. Since texture look-ups
are very cheap on the GPU, it is quite efficient to map the particle position into a 2D or 3D
texture containing flow velocity vectors. This sampled flow vector v fl can be used with Stoke's
law of a drag force Fd on a sphere:

Dampening:
– Scale down velocity vector
– Simulates slow down in viscous materials
 Un-dampening:
– Scale up velocity vector
– Simulates self-moving objects, e.g. bee swarm
*/

// position invariant, gravity etc.
class globalForce
{
public:
	globalForce(){};
	~globalForce(){};

	float m_strength;
	CVector<float,3> m_direction;
	float m_startTime;
private:
};


class localForce: public globalForce
{
public:
	localForce(){};
	~localForce(){};

	float m_maxDistance;
	CVector<float,3> m_position;
private:
};

#endif