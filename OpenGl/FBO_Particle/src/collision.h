#ifndef collision_H
#define collision_H


#include <vector>
#include "Vector.h"
/*
Collision on GPU limited for efficiency:
� Primitive objects: sphere, plane, box
� Texture-based height fields, i.e. terrain (might be dynamic!)
 Algorithm:
1. Detect collision with expected new position
2. Determine surface normal at approximate
penetration point
3. React on collision, i.e. alter velocity
*/
//floor

class collisionObject
{
public:
	collisionObject(){};
	~collisionObject(){};
};

#endif