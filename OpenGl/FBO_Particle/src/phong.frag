
uniform vec4 fvAmbient;
uniform vec4 fvSpecular;
uniform vec4 fvDiffuse;
uniform float fSpecularPower;
uniform vec4 fvBaseColor;
//uniform sampler2D baseMap;

varying vec3 ViewDirection;
varying vec3 Normal;
varying vec4 color;

void main( void )
{
   vec3  fvLightDirection = normalize(gl_LightSource[0].position.xyz); //normalize(LightDirection);
   vec3  fvNormal         = normalize(Normal);
   float fNDotL           = max(0.0, dot(fvNormal, fvLightDirection)); 
   
   vec3  fvReflection     = normalize(((2.0*fNDotL) * fvNormal) - fvLightDirection); 
   vec3  fvViewDirection  = normalize(ViewDirection);
   float fRDotV           = max(0.0, dot(fvReflection, fvViewDirection));
   
   vec4  fvTotalAmbient   = fvAmbient * fvBaseColor; 
   vec4  fvTotalDiffuse   = fvDiffuse * fNDotL * fvBaseColor; 
   vec4  fvTotalSpecular  = fvSpecular * (pow(fRDotV, fSpecularPower));
    
   //vec4  fvBaseColor      = texture2D(baseMap, gl_TexCoord[0]);
   //gl_FragColor = (fvBaseColor*fvTotalAmbient + fvBaseColor*fvTotalDiffuse + fvTotalSpecular);
   gl_FragColor = (fvTotalAmbient + fvTotalDiffuse + fvTotalSpecular);
}