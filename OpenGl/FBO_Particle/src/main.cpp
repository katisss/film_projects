////////////////////////////////////////////////////////////////////////
//
//   Harvard Computer Science
//     CS 175: Introduction to Computer Graphics
//   Professor: Hanspeter Pfister
//   Teaching Fellows: Moritz Baecher, Yuanchen Zhu
//
//   File: main.cpp
//   Name: katrin schmid
//   Desc:  draw particles with fbo, glsl
//   
////////////////////////////////////////////////////////////////////////
// Resources 
//http://wiki.delphigl.com/index.php/Tutorial_Framebufferobject (german)
//http://wiki.delphigl.com/index.php/GLSL_Partikel (german)
//http://www.songho.ca/opengl/gl_fbo.html 
//"mega particles" at www.2ld.de/gdc2004/
/////////
// Known problems:
// Coordinates in the float textures arent normalized to positive space

// in order to get function prototypes from glext.h, define GL_GLEXT_PROTOTYPES before including glext.h
#define GL_GLEXT_PROTOTYPES
#define GLEW_STATIC
#include <GL/glew.h>

#ifdef __MAC__
#include <GLUT/glut.h>
#else
#define FREEGLUT_STATIC
#include <GL/glut.h>
#endif

#include "glext.h"
#include <iostream>
#include <sstream>
#include <string>
#include <iomanip>
#include <cstdlib>
#include <vector>

#include "shaders.h"
#include "force.h"
#include "collision.h"
#include "particle.h"

using std::stringstream;
using std::string;
using std::cout;
using std::endl;
using std::ends;


// GLUT CALLBACK functions ////////////////////////////////////////////////////
void displayCB();
void reshapeCB(int w, int h);
void idleCB();
void keyboardCB(unsigned char key, int x, int y);
void mouseCB(int button, int stat, int x, int y);
void mouseMotionCB(int x, int y);
void initMenus();

void initGL();
int  initGLUT(int argc, char **argv);
bool initSharedMem();
void clearSharedMem();
void initLights();
void setCamera(float posX, float posY, float posZ, float targetX, float targetY, float targetZ);
void drawString(const char *str, int x, int y, float color[4], void *font);
void drawString3D(const char *str, float pos[3], float color[4], void *font);
void showInfo();
void showFPS();

void printFramebufferInfo();

std::string convertInternalFormatToString(GLenum format);
std::string getTextureParameters(GLuint id);
std::string getRenderbufferParameters(GLuint id);

void checkGLErrors() {
   const GLenum errCode = glGetError(); // check for errors
   if (errCode != GL_NO_ERROR) {
      std::cerr << "main Error: " << gluErrorString(errCode) << std::endl;
   //   throw runtime_error((const char*)gluErrorString(errCode));
   }
}

// constants
const int SCREEN_WIDTH = 400;
const int SCREEN_HEIGHT = 300;

// global variables
void *font = GLUT_BITMAP_8_BY_13;

bool mouseLeftDown;
bool mouseRightDown;
float mouseX, mouseY;
float cameraAngleX;
float cameraAngleY;
float cameraDistance;
int screenWidth, screenHeight;
bool fboSupported;
bool fboUsed;
int drawMode = 0;

// menu flags
bool gAnimateEmitter = 0;
bool gDisplayDebug = 1;
bool gToggleGroundCollision = 1;
bool gDamp = 0;
bool gLocalForce = 1;
int gTexture = 0;
int gToogleActiveEmitter = 0;

// function pointers for FBO extension
// Windows needs to get function pointers from ICD OpenGL drivers,
// because opengl32.dll does not support extensions higher than v1.1.

// Framebuffer object
PFNGLGENFRAMEBUFFERSEXTPROC                     pglGenFramebuffersEXT = 0;                      // FBO name generation procedure
PFNGLDELETEFRAMEBUFFERSEXTPROC                  pglDeleteFramebuffersEXT = 0;                   // FBO deletion procedure
PFNGLBINDFRAMEBUFFEREXTPROC                     pglBindFramebufferEXT = 0;                      // FBO bind procedure
PFNGLCHECKFRAMEBUFFERSTATUSEXTPROC              pglCheckFramebufferStatusEXT = 0;               // FBO completeness test procedure
PFNGLGETFRAMEBUFFERATTACHMENTPARAMETERIVEXTPROC pglGetFramebufferAttachmentParameterivEXT = 0;  // return various FBO parameters
PFNGLGENERATEMIPMAPEXTPROC                      pglGenerateMipmapEXT = 0;                       // FBO automatic mipmap generation procedure
PFNGLFRAMEBUFFERTEXTURE2DEXTPROC                pglFramebufferTexture2DEXT = 0;                 // FBO texdture attachement procedure
PFNGLFRAMEBUFFERRENDERBUFFEREXTPROC             pglFramebufferRenderbufferEXT = 0;              // FBO renderbuffer attachement procedure
// Renderbuffer object
PFNGLGENRENDERBUFFERSEXTPROC                    pglGenRenderbuffersEXT = 0;                     // renderbuffer generation procedure
PFNGLDELETERENDERBUFFERSEXTPROC                 pglDeleteRenderbuffersEXT = 0;                  // renderbuffer deletion procedure
PFNGLBINDRENDERBUFFEREXTPROC                    pglBindRenderbufferEXT = 0;                     // renderbuffer bind procedure
PFNGLRENDERBUFFERSTORAGEEXTPROC                 pglRenderbufferStorageEXT = 0;                  // renderbuffer memory allocation procedure
PFNGLGETRENDERBUFFERPARAMETERIVEXTPROC          pglGetRenderbufferParameterivEXT = 0;           // return various renderbuffer parameters
PFNGLISRENDERBUFFEREXTPROC                      pglIsRenderbufferEXT = 0;                       // determine renderbuffer object type

#define glGenFramebuffersEXT                        pglGenFramebuffersEXT
#define glDeleteFramebuffersEXT                     pglDeleteFramebuffersEXT
#define glBindFramebufferEXT                        pglBindFramebufferEXT
#define glCheckFramebufferStatusEXT                 pglCheckFramebufferStatusEXT
#define glGetFramebufferAttachmentParameterivEXT    pglGetFramebufferAttachmentParameterivEXT
#define glGenerateMipmapEXT                         pglGenerateMipmapEXT
#define glFramebufferTexture2DEXT                   pglFramebufferTexture2DEXT
#define glFramebufferRenderbufferEXT                pglFramebufferRenderbufferEXT

#define glGenRenderbuffersEXT                       pglGenRenderbuffersEXT
#define glDeleteRenderbuffersEXT                    pglDeleteRenderbuffersEXT
#define glBindRenderbufferEXT                       pglBindRenderbufferEXT
#define glRenderbufferStorageEXT                    pglRenderbufferStorageEXT
#define glGetRenderbufferParameterivEXT             pglGetRenderbufferParameterivEXT
#define glIsRenderbufferEXT                         pglIsRenderbufferEXT

// R E N D E R S T A T E S /////////////////////////////////////////////
struct GlslProgram {
   // Handle to the OpenGL shading program object
   GLuint program;

   // Handles to uniform variables for the shading program
   GLuint fvLightPositionHandle;
   GLuint fvEyePositionHandle;
   GLuint fvAmbientHandle;
   GLuint fvSpecularHandle;
   GLuint fvDiffuseHandle;
   GLuint fSpecularPowerHandle;
   GLuint fvBaseColorHandle;
};


static const int NUM_SHADERS = 4;
GLuint shaderProgramHandles[ NUM_SHADERS ];
static char* glslSources[NUM_SHADERS][2] = {
                        {"phong.vert", "phong.frag"},
                        {"aSimulation.vert", "aSimulation.frag"},
                        {"aRender.vert", "aRender.frag"},
                        {"Textured.vert", "Textured.frag"},
      };
// Vector of structs storing OpenGL program handles and uniform variable handles
static std::vector<GlslProgram> glslPrograms;

// Which shader to use, in the range [0, NUM_SHADES)
static int activeShader = 0; 

// Values of uniform parameter. Note that to pass them to the
// shader program, you need to explicit call glUniform*
static float fvLightPosition[] = {-100.f, 100.f, 100.f};
static float fvEyePosition[] = {0.f, 0.f, 0.f};
static float fvDiffuse[] = {0.88f, 0.88f, 0.88f, 1.0f};
static float fvSpecular[] = {0.8f, 0.8f, 0.8f, 1.0f};
static float fvAmbient[] = {0.37f, 0.37f, 0.37f, 1.0f};
static float fSpecularPower[] = {25};
static float fvBaseColor[] = {0.7f, 0.7f, 0.0f, 1.0f};
static float fvSelectedColor[] = {0.f, 1.f, 0.0f, 1.0f};

ParticleSystem ps;


// Draw a grid of lines
void drawWireGrid(int rows, int cols)
{
   int i;
   glLineWidth(1.0);

   glBegin(GL_LINES);
   for(i = 0; i <= rows; i++) {
      glVertex3f(0.0, 0.0,(float) i / rows);
      glVertex3f(1.0, 0.0,(float) i / rows);
   }
   for(i = 0; i<= cols; i++) {
      glVertex3f((float)i / cols, 0.0, 0.0);
      glVertex3f((float)i / cols, 0.0, 1.0);
   }
   glEnd();
}


////////////////////////// define particle system
ParticleSystem createParticlePreset(int whichPreset)
{
   ps.reset();
   ps.m_pEmitters.clear();
   ps.m_gforces.clear();
   ps.m_collision.clear();
   ps.m_lforces.clear();

   if (whichPreset == 0)
   {
      ParticleEmitter pe1, pe2;
      // emitter 1
      pe1.m_position = (CVector<float,3>(0., 2.5, -2.));
      pe1.m_positionMaxOffset = CVector<float,3>(.12, -.05, -.14);
      pe1.m_initialDirection = CVector<float,3>(0.2, 0., 0.);// unused

      pe1.m_initialSizeMinMax[0] = 0.1;
      pe1.m_initialSizeMinMax[0] = 1.1;
      pe1.m_initialSpeedMinMax[0] = 0.;// unused
      pe1.m_initialSpeedMinMax[0] = .2;
      pe1.m_lifeTimeMinMax[0] = 1.;
      pe1.m_lifeTimeMinMax[1] = 3.;
       pe1.m_numOfNewParticlesMinMax[0] = 2;
      pe1.m_numOfNewParticlesMinMax[1] = 4;
      ps.addEmitter(pe1);

      // emitter 2
      pe2.m_position = (CVector<float,3>(2., 2, -.2));
      pe2.m_positionMaxOffset = CVector<float,3>(.15, -.02, .13);
      pe2.m_initialDirection = CVector<float,3>(0.2, 0., 0.);// unused

      pe2.m_initialSizeMinMax[0] = 0.1;
      pe2.m_initialSizeMinMax[0] = 1.1;
      pe2.m_initialSpeedMinMax[0] = 0.;// unused
      pe2.m_initialSpeedMinMax[0] = .2;
      pe2.m_lifeTimeMinMax[0] = 1.;
      pe2.m_lifeTimeMinMax[1] = 4.;
       pe2.m_numOfNewParticlesMinMax[0] = 1;
      pe2.m_numOfNewParticlesMinMax[1] = 3;
      ps.addEmitter(pe2);

      // global force
      globalForce gf;
      gf.m_direction = CVector<float,3>(0.1, -.5, 0.1);
      gf.m_strength = 0.02;
      ps.addGlobalForce(gf);

      // local force
      localForce lf;
      lf.m_maxDistance = 5;
      lf. m_strength = 0.01;
      lf.m_direction = CVector<float,3>(0, -1., 0.1);
      lf.m_position = CVector<float,3>(0, -1., 0.1);
      ps.addLocalForce(lf);

      collisionObject co;
      ps.addCollision(co);
      //std::cout <<"shaderProgramHandles[0]: "<<shaderProgramHandles[0]<<shaderProgramHandles[1]<<shaderProgramHandles[2]<<std::endl;
      ps.setUp(1, 0., shaderProgramHandles[1], shaderProgramHandles[2], shaderProgramHandles[3]);
   }
   return ps;
}

GLuint initPhongProgram(GlslProgram& program, const char* vsFile, const char *fsFile)
{
   GLuint p = compileShaders(vsFile, fsFile);
   program.program = p;

   // Grab handle to the compiled GLSL program and store them for later use.
   program.fvLightPositionHandle = glGetUniformLocation(p, "fvLightPosition");
   program.fvEyePositionHandle = glGetUniformLocation(p, "fvEyePosition");
   program.fvAmbientHandle = glGetUniformLocation(p, "fvAmbient");
   program.fvSpecularHandle = glGetUniformLocation(p, "fvSpecular");
   program.fvDiffuseHandle = glGetUniformLocation(p, "fvDiffuse");
   program.fSpecularPowerHandle = glGetUniformLocation(p, "fSpecularPower");
   program.fvBaseColorHandle = glGetUniformLocation(p, "fvBaseColor");

   return p;
}

void initGLSL()
{
   const GLubyte *str;
   str = glGetString (GL_SHADING_LANGUAGE_VERSION);
   glslPrograms.resize(NUM_SHADERS);
   shaderProgramHandles[0] = initPhongProgram(glslPrograms[0], glslSources[0][0], glslSources[0][1]);

   for ( int shaderIndex = 1; shaderIndex < NUM_SHADERS; ++shaderIndex)
   {
       GLuint shaderProgramHandle = compileShaders( glslSources[ shaderIndex ][ 0 ], glslSources[ shaderIndex ][ 1 ]);
       shaderProgramHandles[ shaderIndex ] = shaderProgramHandle;
       cout << shaderIndex << " initGLSL "<<shaderProgramHandles[ shaderIndex ]<< glslSources[ shaderIndex ][ 0 ]<<endl;
   }
 
   checkGLErrors();
}


///////////////////////////////////////////////////////////////////////////////
int main(int argc, char **argv)
{
    initSharedMem();
    // init GLUT and GL
    initGLUT(argc, argv);
    glewInit();
    initGL();
    initMenus();

   // get pointers to GL functions
        glGenFramebuffersEXT                    = (PFNGLGENFRAMEBUFFERSEXTPROC)wglGetProcAddress("glGenFramebuffersEXT");
        glDeleteFramebuffersEXT                 = (PFNGLDELETEFRAMEBUFFERSEXTPROC)wglGetProcAddress("glDeleteFramebuffersEXT");
        glBindFramebufferEXT                    = (PFNGLBINDFRAMEBUFFEREXTPROC)wglGetProcAddress("glBindFramebufferEXT");
        glCheckFramebufferStatusEXT             = (PFNGLCHECKFRAMEBUFFERSTATUSEXTPROC)wglGetProcAddress("glCheckFramebufferStatusEXT");
        glGetFramebufferAttachmentParameterivEXT = (PFNGLGETFRAMEBUFFERATTACHMENTPARAMETERIVEXTPROC)wglGetProcAddress("glGetFramebufferAttachmentParameterivEXT");
        glGenerateMipmapEXT                     = (PFNGLGENERATEMIPMAPEXTPROC)wglGetProcAddress("glGenerateMipmapEXT");
        glFramebufferTexture2DEXT               = (PFNGLFRAMEBUFFERTEXTURE2DEXTPROC)wglGetProcAddress("glFramebufferTexture2DEXT");
        glFramebufferRenderbufferEXT            = (PFNGLFRAMEBUFFERRENDERBUFFEREXTPROC)wglGetProcAddress("glFramebufferRenderbufferEXT");
        glGenRenderbuffersEXT                   = (PFNGLGENRENDERBUFFERSEXTPROC)wglGetProcAddress("glGenRenderbuffersEXT");
        glDeleteRenderbuffersEXT                = (PFNGLDELETERENDERBUFFERSEXTPROC)wglGetProcAddress("glDeleteRenderbuffersEXT");
        glBindRenderbufferEXT                   = (PFNGLBINDRENDERBUFFEREXTPROC)wglGetProcAddress("glBindRenderbufferEXT");
        glRenderbufferStorageEXT                = (PFNGLRENDERBUFFERSTORAGEEXTPROC)wglGetProcAddress("glRenderbufferStorageEXT");
        glGetRenderbufferParameterivEXT         = (PFNGLGETRENDERBUFFERPARAMETERIVEXTPROC)wglGetProcAddress("glGetRenderbufferParameterivEXT");
        glIsRenderbufferEXT                     = (PFNGLISRENDERBUFFEREXTPROC)wglGetProcAddress("glIsRenderbufferEXT");

      // check once again FBO extension
        if (glGenFramebuffersEXT && glDeleteFramebuffersEXT && glBindFramebufferEXT && glCheckFramebufferStatusEXT &&
           glGetFramebufferAttachmentParameterivEXT && glGenerateMipmapEXT && glFramebufferTexture2DEXT && glFramebufferRenderbufferEXT &&
           glGenRenderbuffersEXT && glDeleteRenderbuffersEXT && glBindRenderbufferEXT && glRenderbufferStorageEXT &&
           glGetRenderbufferParameterivEXT && glIsRenderbufferEXT)
        {
            fboSupported = fboUsed = true;
            cout << "Video card supports GL_EXT_framebuffer_object." << endl;
        }
        else
        {
            fboSupported = fboUsed = false;
            cout << "Video card does NOT support GL_EXT_framebuffer_object." << endl;
        }

    // start timer, the elapsed time will be used for rotating the teapot
    createParticlePreset(0);
    glutMainLoop(); /* Start GLUT event-processing loop */

    return 0;
}


///////////////////////////////////////////////////////////////////////////////
// initialize GLUT for windowing
///////////////////////////////////////////////////////////////////////////////
int initGLUT(int argc, char **argv)
{
    glutInit(&argc, argv);

    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_ALPHA);   // display mode
    glutInitWindowSize(screenWidth, screenHeight);              // window size
    glutInitWindowPosition(100, 100);                           // window location
    int handle = glutCreateWindow(argv[0]);     // param is the title of window

    // register GLUT callback functions
    glutDisplayFunc(displayCB);
    glutIdleFunc(idleCB);                       // redraw whenever system is idle
    glutReshapeFunc(reshapeCB);
    glutKeyboardFunc(keyboardCB);
    glutMouseFunc(mouseCB);
    glutMotionFunc(mouseMotionCB);

    return handle;
}



///////////////////////////////////////////////////////////////////////////////
// initialize OpenGL
void initGL()
{
    initGLSL();
    glUseProgram(0);

    glShadeModel(GL_SMOOTH);                    // shading mathod: GL_SMOOTH or GL_FLAT
    glPixelStorei(GL_UNPACK_ALIGNMENT, 4);      // 4-byte pixel alignment

    // enable /disable features
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    //glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
    //glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHTING);
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_CULL_FACE);

     // track material ambient and diffuse from surface color, call it before glEnable(GL_COLOR_MATERIAL)
    glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
    glEnable(GL_COLOR_MATERIAL);

    glClearColor(0., 0., 0., 0.);                   // background color
    glClearStencil(0);                          // clear stencil buffer
    glClearDepth(1.0f);                         // 0 is near, 1 is far
    glDepthFunc(GL_LEQUAL);

    initLights();
    setCamera(0, 0, 6, 0, 0, 0);


   checkGLErrors();
}



///////////////////////////////////////////////////////////////////////////////
// write 2d text using GLUT
// The projection matrix must be set to orthogonal before call this function.
///////////////////////////////////////////////////////////////////////////////
void drawString(const char *str, int x, int y, float color[4], void *font)
{
    glPushAttrib(GL_LIGHTING_BIT | GL_CURRENT_BIT);// lighting and color mask
    glDisable(GL_LIGHTING); // need to disable lighting for proper text color
    glDisable(GL_TEXTURE_2D);

    glColor4fv(color);          // set text color
    glRasterPos2i(x, y);        // place text position

    // loop all characters in the string
    while(*str)
    {
        glutBitmapCharacter(font, *str);
        ++str;
    }

    glEnable(GL_TEXTURE_2D);
    glEnable(GL_LIGHTING);
    glPopAttrib();
}




///////////////////////////////////////////////////////////////////////////////
bool initSharedMem()
{
    screenWidth = SCREEN_WIDTH;
    screenHeight = SCREEN_HEIGHT;
    mouseLeftDown = mouseRightDown = false;
    cameraAngleX = cameraAngleY = 45;
    cameraDistance = 0;

    return true;
}


void clearSharedMem()
{}


///////////////////////////////////////////////////////////////////////////////
// initialize lights
///////////////////////////////////////////////////////////////////////////////
void initLights()
{
    // set up light colors (ambient, diffuse, specular)
    GLfloat lightKa[] = {.2f, .2f, .2f, 1.0f};  // ambient light
    GLfloat lightKd[] = {.7f, .7f, .7f, 1.0f};  // diffuse light
    GLfloat lightKs[] = {1, 1, 1, 1};           // specular light
    glLightfv(GL_LIGHT0, GL_AMBIENT, lightKa);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, lightKd);
    glLightfv(GL_LIGHT0, GL_SPECULAR, lightKs);

    // position the light
    float lightPos[4] = {0, 0, 20, 1}; // positional light
    glLightfv(GL_LIGHT0, GL_POSITION, lightPos);

    glEnable(GL_LIGHT0);                        // MUST enable each light source after configuration
}



///////////////////////////////////////////////////////////////////////////////
// set camera position and lookat direction
///////////////////////////////////////////////////////////////////////////////
void setCamera(float posX, float posY, float posZ, float targetX, float targetY, float targetZ)
{
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(posX, posY, posZ, targetX, targetY, targetZ, 0, 1, 0); // eye(x,y,z), focal(x,y,z), up(x,y,z)
}



///////////////////////////////////////////////////////////////////////////////
// display info messages
///////////////////////////////////////////////////////////////////////////////
void showInfo()
{
    // backup current model-view matrix
    glPushMatrix();                     // save current modelview matrix
    glLoadIdentity();                   // reset modelview matrix

    // set to 2D orthogonal projection
    glMatrixMode(GL_PROJECTION);        // switch to projection matrix
    glPushMatrix();                     // save current projection matrix
    glLoadIdentity();                   // reset projection matrix
    gluOrtho2D(0, screenWidth, 0, screenHeight);  // set to orthogonal projection

    const int FONT_HEIGHT = 14;
    float color[4] = {1, 1, 1, 1};

    stringstream ss;
    ss << "FBO: ";
    if(fboUsed)
        ss << "on" << ends;
    else
        ss << "off" << ends;

    drawString(ss.str().c_str(), 1, screenHeight-FONT_HEIGHT, color, font);
    ss.str(""); // clear buffer

    ss << std::fixed << std::setprecision(3);
//    ss << "Render-To-Texture Time: " << renderToTextureTime << " ms" << ends;
    drawString(ss.str().c_str(), 1, screenHeight-(2*FONT_HEIGHT), color, font);
    ss.str("");

    ss << "Press SPACE to toggle FBO." << ends;
    drawString(ss.str().c_str(), 1, 1, color, font);

    // unset floating format
    ss << std::resetiosflags(std::ios_base::fixed | std::ios_base::floatfield);

    // restore projection matrix
    glPopMatrix();                   // restore to previous projection matrix

    // restore modelview matrix
    glMatrixMode(GL_MODELVIEW);      // switch to modelview matrix
    glPopMatrix();                   // restore to previous modelview matrix
}




///////////////////////////////////////////////////////////////////////////////
// print out the FBO infos
///////////////////////////////////////////////////////////////////////////////
void printFramebufferInfo()
{
    cout << "\n***** FBO STATUS *****\n";

    // print max # of colorbuffers supported by FBO
    int colorBufferCount = 0;
    glGetIntegerv(GL_MAX_COLOR_ATTACHMENTS_EXT, &colorBufferCount);
    cout << "Max Number of Color Buffer Attachment Points: " << colorBufferCount << endl;

    int objectType;
    int objectId;

    // print info of the colorbuffer attachable image
    for(int i = 0; i < colorBufferCount; ++i)
    {
        glGetFramebufferAttachmentParameterivEXT(GL_FRAMEBUFFER_EXT,
                                                 GL_COLOR_ATTACHMENT0_EXT+i,
                                                 GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE_EXT,
                                                 &objectType);
        if(objectType != GL_NONE)
        {
            glGetFramebufferAttachmentParameterivEXT(GL_FRAMEBUFFER_EXT,
                                                     GL_COLOR_ATTACHMENT0_EXT+i,
                                                     GL_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME_EXT,
                                                     &objectId);

            std::string formatName;

            cout << "Color Attachment " << i << ": ";
            if(objectType == GL_TEXTURE)
                cout << "GL_TEXTURE, " << getTextureParameters(objectId) << endl;
            else if(objectType == GL_RENDERBUFFER_EXT)
                cout << "GL_RENDERBUFFER_EXT, " << getRenderbufferParameters(objectId) << endl;
        }
    }

    // print info of the depthbuffer attachable image
    glGetFramebufferAttachmentParameterivEXT(GL_FRAMEBUFFER_EXT,
                                             GL_DEPTH_ATTACHMENT_EXT,
                                             GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE_EXT,
                                             &objectType);
    if(objectType != GL_NONE)
    {
        glGetFramebufferAttachmentParameterivEXT(GL_FRAMEBUFFER_EXT,
                                                 GL_DEPTH_ATTACHMENT_EXT,
                                                 GL_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME_EXT,
                                                 &objectId);

        cout << "Depth Attachment: ";
        switch(objectType)
        {
        case GL_TEXTURE:
            cout << "GL_TEXTURE, " << getTextureParameters(objectId) << endl;
            break;
        case GL_RENDERBUFFER_EXT:
            cout << "GL_RENDERBUFFER_EXT, " << getRenderbufferParameters(objectId) << endl;
            break;
        }
    }

    // print info of the stencilbuffer attachable image
    glGetFramebufferAttachmentParameterivEXT(GL_FRAMEBUFFER_EXT,
                                             GL_STENCIL_ATTACHMENT_EXT,
                                             GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE_EXT,
                                             &objectType);
    if(objectType != GL_NONE)
    {
        glGetFramebufferAttachmentParameterivEXT(GL_FRAMEBUFFER_EXT,
                                                 GL_STENCIL_ATTACHMENT_EXT,
                                                 GL_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME_EXT,
                                                 &objectId);

        cout << "Stencil Attachment: ";
        switch(objectType)
        {
        case GL_TEXTURE:
            cout << "GL_TEXTURE, " << getTextureParameters(objectId) << endl;
            break;
        case GL_RENDERBUFFER_EXT:
            cout << "GL_RENDERBUFFER_EXT, " << getRenderbufferParameters(objectId) << endl;
            break;
        }
    }

    cout << endl;
}



///////////////////////////////////////////////////////////////////////////////
// return texture parameters as string using glGetTexLevelParameteriv()
///////////////////////////////////////////////////////////////////////////////
std::string getTextureParameters(GLuint id)
{
    if(glIsTexture(id) == GL_FALSE)
        return "Not texture object";

    int width, height, format;
    std::string formatName;
    glBindTexture(GL_TEXTURE_2D, id);
    glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, &width);            // get texture width
    glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, &height);          // get texture height
    glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_INTERNAL_FORMAT, &format); // get texture internal format
    glBindTexture(GL_TEXTURE_2D, 0);

    formatName = convertInternalFormatToString(format);

    std::stringstream ss;
    ss << width << "x" << height << ", " << formatName;
    return ss.str();
}



///////////////////////////////////////////////////////////////////////////////
// return renderbuffer parameters as string using glGetRenderbufferParameterivEXT
///////////////////////////////////////////////////////////////////////////////
std::string getRenderbufferParameters(GLuint id)
{
    if(glIsRenderbufferEXT(id) == GL_FALSE)
        return "Not Renderbuffer object";

    int width, height, format;
    std::string formatName;
    glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, id);
    glGetRenderbufferParameterivEXT(GL_RENDERBUFFER_EXT, GL_RENDERBUFFER_WIDTH_EXT, &width);    // get renderbuffer width
    glGetRenderbufferParameterivEXT(GL_RENDERBUFFER_EXT, GL_RENDERBUFFER_HEIGHT_EXT, &height);  // get renderbuffer height
    glGetRenderbufferParameterivEXT(GL_RENDERBUFFER_EXT, GL_RENDERBUFFER_INTERNAL_FORMAT_EXT, &format); // get renderbuffer internal format
    glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, 0);

    formatName = convertInternalFormatToString(format);

    std::stringstream ss;
    ss << width << "x" << height << ", " << formatName;
    return ss.str();
}


// back to normal viewport and projection matrix
void render()
{

   // back to normal viewport and projection matrix
    glViewport(0, 0, screenWidth, screenHeight);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.0f, (float)(screenWidth)/screenHeight, 1.0f, 100.0f);
    glMatrixMode(GL_MODELVIEW);

    // clear framebuffer
    glClearColor(0, 0, 0, 0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

glPushMatrix();

    // tramsform camera
    glTranslatef(0, 0, cameraDistance);
    glRotatef(cameraAngleX, 1, 0, 0);   // pitch
    glRotatef(cameraAngleY, 0, 1, 0);   // heading

//////////////////////////////// draw
   glEnable(GL_LIGHTING);
    glEnable(GL_COLOR_MATERIAL);
   glEnable(GL_LIGHT1);

   const GlslProgram& p = glslPrograms[0];
   glUseProgram(shaderProgramHandles[0]);
   glUniform3fv(p.fvLightPositionHandle, 1, fvLightPosition);
   glUniform3fv(p.fvEyePositionHandle, 1, fvEyePosition);
   glUniform4fv(p.fvAmbientHandle, 1, fvAmbient);
   glUniform4fv(p.fvSpecularHandle, 1, fvSpecular);
   glUniform4fv(p.fvDiffuseHandle, 1, fvDiffuse);
   glUniform1fv(p.fSpecularPowerHandle, 1, fSpecularPower);
   glUniform4fv(p.fvBaseColorHandle, 1, fvBaseColor);
   glBindTexture(GL_TEXTURE_2D, 0);

glPushMatrix();

   glScalef(8.,8.,8.);
   glTranslatef(-.5,  0 ,-.5);
   drawWireGrid(50, 50);
   //ps.displayGround(200, 200);
glPopMatrix();

glPushMatrix();
   for (int i = 0; i < ps.m_pEmitters.size(); i++)
   {
      glPushMatrix();
      if (i == ps.m_selectedEmitter)
         glUniform4fv(p.fvBaseColorHandle, 1, fvSelectedColor);
      else
         glUniform4fv(p.fvBaseColorHandle, 1, fvBaseColor);
      ps.drawEmitter(i);
      glPopMatrix();
   }
   glUniform4fv(p.fvBaseColorHandle, 1, fvBaseColor);
glPopMatrix();


   if (gDisplayDebug == 1)
   {
      glLineWidth(3);
      glBegin(GL_LINES);
         glVertex3f(.0f, .0f, .0f);
         glVertex3f(ps.m_gforces[0].m_direction[0]*8., ps.m_gforces[0].m_direction[1]*8., ps.m_gforces[0].m_direction[2]*8.); // ending point of the line
      glEnd();

glPushMatrix();
      ps.displayDebug(gTexture);
glPopMatrix();
   }

glPushMatrix();
   ps.displayParticles();
glPopMatrix();

glPopMatrix();

   glutSwapBuffers();

   //checkGLErrors();

}


// =============================================================================
void displayCB()
{
   glPushAttrib(GL_VIEWPORT_BIT);
   glPushMatrix();

   if (gAnimateEmitter == 1)
      ps.animateEmitter(ps.m_selectedEmitter, CVector<float,3>(.1,0.,.1), 0.02);

   ps.updateParticles(0.09);//ps.m_timeStep);
   glPopMatrix();
    glPopAttrib;

   render();
}

void reshapeCB(int width, int height)
{
    screenWidth = width;
    screenHeight = height;

    // set viewport to be the entire window
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);

    // set perspective viewing frustum
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.0f, (float)(width)/height, 1.0f, 1000.0f); // FOV, AspectRatio, NearClip, FarClip

    // switch to modelview matrix in order to set scene
    glMatrixMode(GL_MODELVIEW);
}


void idleCB()
{
    glutPostRedisplay();
}


void keyboardCB(unsigned char key, int x, int y)
{
    switch(key)
    {
    case 27: // ESCAPE
        exit(0);
        break;

    case 'd': // switch rendering modes (fill -> wire -> point)
    case 'D':
        drawMode = ++drawMode % 3;
        if(drawMode == 0)        // fill mode
        {
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            glEnable(GL_DEPTH_TEST);
            glEnable(GL_CULL_FACE);
        }
        else if(drawMode == 1)  // wireframe mode
        {
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            glDisable(GL_DEPTH_TEST);
            glDisable(GL_CULL_FACE);
        }
        else                    // point mode
        {
            glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
            glDisable(GL_DEPTH_TEST);
            glDisable(GL_CULL_FACE);
        }
        break;
   case 'w':
      //cout <<"mselected: "<< ps.m_selectedEmitter << ps.m_pEmitters.size()<< endl;
      if (ps.m_selectedEmitter > = ps.m_pEmitters.size()-1)
         ps.m_selectedEmitter = 0;
      else
         ps.m_selectedEmitter += 1;
   case 'x':
      gToogleActiveEmitter = !gToogleActiveEmitter;
      if (gToogleActiveEmitter == 1)
      {
         ps.m_pEmitters[ps.m_selectedEmitter].m_numOfNewParticlesMinMax[0] = 0;
         ps.m_pEmitters[ps.m_selectedEmitter].m_numOfNewParticlesMinMax[1] = 0;
      }
      else
      {
         ps.m_pEmitters[ps.m_selectedEmitter].m_numOfNewParticlesMinMax[0] = 2;
         ps.m_pEmitters[ps.m_selectedEmitter].m_numOfNewParticlesMinMax[1] = 7;
      }
      break;
   case 'n':
       for (int i = 0; i < ps.m_gforces.size();i++) //Increase
          ps.m_gforces[i].m_strength = ps.m_gforces[i].m_strength*3;
      break;

   case 'l':
       for (int i = 0; i < ps.m_gforces.size(); i++)
          ps.m_gforces[i].m_strength -= ps.m_gforces[i].m_strength/3;
      break;
   case 's':
       for (int i = 0; i<ps.m_pEmitters.size();i++)
       {
         ps.m_pEmitters[i].m_numOfNewParticlesMinMax[0] = ps.m_pEmitters[i].m_lifeTimeMinMax[0] *2.;
         ps.m_pEmitters[i].m_numOfNewParticlesMinMax[0] = ps.m_pEmitters[i].m_lifeTimeMinMax[1] *2.;
       }
      break;
   case 'q':
       for (int i = 0; i<ps.m_pEmitters.size();i++)
       {
          ps.m_pEmitters[i].m_numOfNewParticlesMinMax[0] = ps.m_pEmitters[i].m_lifeTimeMinMax[0]/2.;
          ps.m_pEmitters[i].m_numOfNewParticlesMinMax[1] = ps.m_pEmitters[i].m_lifeTimeMinMax[1]/2.;
       }
      break;
   case 'b':
         if (gTexture == 0 || gTexture == 2)
            gTexture = 1;
         else
            gTexture = 2;
      break;
   case 'c':
         gTexture = 2;
      break;
   case 'a':
         gAnimateEmitter = !gAnimateEmitter;
      break;
   case 'g':
         ps.mirrorGlForceDirection(0, ps.m_gforces[0].m_direction);
      break;

   case 'e':
         gDisplayDebug = !gDisplayDebug;
      break;

   case 'p':         //Double emission rates
       for (int i = 0; i < ps.m_pEmitters.size(); i++)
         ps.m_pEmitters[i].m_numOfNewParticlesMinMax = ps.m_pEmitters[i].m_numOfNewParticlesMinMax*2.;
   break;

   case 't':         //Half emission rates
       for (int i = 0; i < ps.m_pEmitters.size(); i++)
         ps.m_pEmitters[i].m_numOfNewParticlesMinMax = ps.m_pEmitters[i].m_numOfNewParticlesMinMax/2.;
      break;

   case 'o':
         gToggleGroundCollision = !gToggleGroundCollision;
         if (gToggleGroundCollision)
            ps.m_groundPlane = 0.;
         else
            ps.m_groundPlane = -1000.;
         break;

   case 'y':
       if (ps.m_selectedEmitter <= ps.m_pEmitters.size())
         ps.m_selectedEmitter += 1;
       else
         ps.m_selectedEmitter = 0;
      break;

   case 'f':
         gDamp = !gDamp;
      break;

   case 'r':
         createParticlePreset(0);
      break;
   case 'k':
      if (ps.m_drawAs < 1)
         ps.m_drawAs += 1;
      else
         ps.m_drawAs = 0;
   break;


   case 'i':// Increase emitter radius
          ps.m_pEmitters[ps.m_selectedEmitter].m_positionMaxOffset[1] = ps.m_pEmitters[ps.m_selectedEmitter].m_positionMaxOffset[1]*1.1;
      break;

   case 'z'://Decrease emitter radius
          ps.m_pEmitters[ps.m_selectedEmitter].m_positionMaxOffset[1] = ps.m_pEmitters[ps.m_selectedEmitter].m_positionMaxOffset[1]/1.1;

      break;

   }

   glutPostRedisplay();
}

void mouseCB(int button, int state, int x, int y)
{
    mouseX = x;
    mouseY = y;

    if(button == GLUT_LEFT_BUTTON)
    {
        if(state == GLUT_DOWN)
        {
            mouseLeftDown = true;
        }
        else if(state == GLUT_UP)
            mouseLeftDown = false;
    }

    else if(button == GLUT_RIGHT_BUTTON)
    {
        if(state == GLUT_DOWN)
        {
            mouseRightDown = true;
        }
        else if(state == GLUT_UP)
            mouseRightDown = false;
    }
}


void mouseMotionCB(int x, int y)
{
    if(mouseLeftDown)
    {
        cameraAngleY += (x - mouseX);
        cameraAngleX += (y - mouseY);
        mouseX = x;
        mouseY = y;
    }
    if(mouseRightDown)
    {
        cameraDistance += (y - mouseY) * 0.2f;
        mouseY = y;
    }
}


void exitCB()
{
    clearSharedMem();
}



///////////////////////////////////////////////////////////////////////////////
// convert OpenGL internal format enum to string
///////////////////////////////////////////////////////////////////////////////
std::string convertInternalFormatToString(GLenum format)
{
    std::string formatName;

    switch(format)
    {
    case GL_STENCIL_INDEX:
        formatName = "GL_STENCIL_INDEX";
        break;
    case GL_DEPTH_COMPONENT:
        formatName = "GL_DEPTH_COMPONENT";
        break;
    case GL_ALPHA:
        formatName = "GL_ALPHA";
        break;
    case GL_RGB:
        formatName = "GL_RGB";
        break;
    case GL_RGBA:
        formatName = "GL_RGBA";
        break;
    case GL_LUMINANCE:
        formatName = "GL_LUMINANCE";
        break;
    case GL_LUMINANCE_ALPHA:
        formatName = "GL_LUMINANCE_ALPHA";
        break;
    case GL_ALPHA4:
        formatName = "GL_ALPHA4";
        break;
    case GL_ALPHA8:
        formatName = "GL_ALPHA8";
        break;
    case GL_ALPHA12:
        formatName = "GL_ALPHA12";
        break;
    case GL_ALPHA16:
        formatName = "GL_ALPHA16";
        break;
    case GL_LUMINANCE4:
        formatName = "GL_LUMINANCE4";
        break;
    case GL_LUMINANCE8:
        formatName = "GL_LUMINANCE8";
        break;
    case GL_LUMINANCE12:
        formatName = "GL_LUMINANCE12";
        break;
    case GL_LUMINANCE16:
        formatName = "GL_LUMINANCE16";
        break;
    case GL_LUMINANCE4_ALPHA4:
        formatName = "GL_LUMINANCE4_ALPHA4";
        break;
    case GL_LUMINANCE6_ALPHA2:
        formatName = "GL_LUMINANCE6_ALPHA2";
        break;
    case GL_LUMINANCE8_ALPHA8:
        formatName = "GL_LUMINANCE8_ALPHA8";
        break;
    case GL_LUMINANCE12_ALPHA4:
        formatName = "GL_LUMINANCE12_ALPHA4";
        break;
    case GL_LUMINANCE12_ALPHA12:
        formatName = "GL_LUMINANCE12_ALPHA12";
        break;
    case GL_LUMINANCE16_ALPHA16:
        formatName = "GL_LUMINANCE16_ALPHA16";
        break;
    case GL_INTENSITY:
        formatName = "GL_INTENSITY";
        break;
    case GL_INTENSITY4:
        formatName = "GL_INTENSITY4";
        break;
    case GL_INTENSITY8:
        formatName = "GL_INTENSITY8";
        break;
    case GL_INTENSITY12:
        formatName = "GL_INTENSITY12";
        break;
    case GL_INTENSITY16:
        formatName = "GL_INTENSITY16";
        break;
    case GL_R3_G3_B2:
        formatName = "GL_R3_G3_B2";
        break;
    case GL_RGB4:
        formatName = "GL_RGB4";
        break;
    case GL_RGB5:
        formatName = "GL_RGB4";
        break;
    case GL_RGB8:
        formatName = "GL_RGB8";
        break;
    case GL_RGB10:
        formatName = "GL_RGB10";
        break;
    case GL_RGB12:
        formatName = "GL_RGB12";
        break;
    case GL_RGB16:
        formatName = "GL_RGB16";
        break;
    case GL_RGBA2:
        formatName = "GL_RGBA2";
        break;
    case GL_RGBA4:
        formatName = "GL_RGBA4";
        break;
    case GL_RGB5_A1:
        formatName = "GL_RGB5_A1";
        break;
    case GL_RGBA8:
        formatName = "GL_RGBA8";
        break;
    case GL_RGB10_A2:
        formatName = "GL_RGB10_A2";
        break;
    case GL_RGBA12:
        formatName = "GL_RGBA12";
        break;
    case GL_RGBA16:
        formatName = "GL_RGBA16";
        break;
    default:
        formatName = "Unknown Format";
    }

    return formatName;
}



void mouse(int button, int state, int x, int y)
{
   int mods;
   /*
   if (state == GLUT_DOWN)
      buttonState | = 1 << button;
   else if (state == GLUT_UP)
      buttonState = 0;

   mods = glutGetModifiers();
   if (mods & GLUT_ACTIVE_SHIFT)
      buttonState = 2;
   else if (mods & GLUT_ACTIVE_CTRL)
      buttonState = 3;

   previousMousePosition = float2(x, y);
   */
   glutPostRedisplay();
}


void mainMenu(int i)
{
   keyboardCB((unsigned char) i, 0, 0);
}

void initMenus(void)
{
   glutCreateMenu(mainMenu);
   glutAddMenuEntry("Cycle selected emitter [w]", 'w');
   glutAddMenuEntry("Toggle selected emitter on off [x]", 'x');
   glutAddMenuEntry("Animate selected emitter [a]", 'a');
   glutAddMenuEntry("Double emission rates [p]", 'p');
   glutAddMenuEntry("Half emission rates [t]", 't');
   glutAddMenuEntry("Double new particles life times [s]", 's');
   glutAddMenuEntry("Half new particles life times [q]", 'q');
   glutAddMenuEntry("Toogle points/sprites [k]", 'k');
   /*
   glutAddMenuEntry("Increase emitter radius [d]", 'd');
   glutAddMenuEntry("Decrease emitter radius [z]", 'z'); // causes drawing problems
   */
   glutAddMenuEntry("-------------------------", ' ');
   glutAddMenuEntry("Reflect global force direction [g]", 'g');
   glutAddMenuEntry("Increase global force strength [n]", 'n');
   glutAddMenuEntry("Decrease global force strength [l]", 'l');
   glutAddMenuEntry("Toggle groundCollision [o]", 'o');
   glutAddMenuEntry("-------------------------", ' ');
   glutAddMenuEntry("Display debug objects [e]", 'e');
   glutAddMenuEntry("Toogle position texture 1, 2 [b]", 'b');
   glutAddMenuEntry("Display attribute texture [c]", 'c');
   glutAddMenuEntry("-------------------------", ' ');
   glutAddMenuEntry("Reset emitter [r]", 'r');
   //glutAddMenuEntry("Toggle local force [m]", 'm');
   glutAttachMenu(GLUT_RIGHT_BUTTON);
}

