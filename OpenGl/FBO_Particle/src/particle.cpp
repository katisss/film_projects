
//#define DEBUG

#define GLEW_STATIC
#include <GL/glew.h>
#ifdef __MAC__
#include <GLUT/glut.h>
#else
# efine FREEGLUT_STATIC
#include <GL/glut.h>
#endif

#include <cmath>
#include <ctime>    // For time()
#include <cstdlib>  // For srand() and rand()
#include <cstdio>
#include <cstring>
#include <iostream>
#include <fstream>
#include "limits.h"

#include "particle.h"

GLuint simProg = 0; // TODO make particle class members
GLuint renderProg = 0;
GLuint debugShaderProg = 0;

// Globals
const int tSize = 256;
double rMax = static_cast<double>(RAND_MAX) + 1.0;
void DrawQuad(float Width, float Height);

inline float frand()
{
    return rand() / (float) RAND_MAX;
}

///////////////////////////////////////////////////////////////////////////////
// check FBO completeness
///////////////////////////////////////////////////////////////////////////////
bool checkFramebufferStatus()
{
    // check FBO status
    GLenum status = glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT);
    switch(status)
    {
    case GL_FRAMEBUFFER_COMPLETE_EXT:
        std::cout << "Framebuffer complete." << std::endl;
        return true;

    case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT_EXT:
        std::cout << "[ERROR] Framebuffer incomplete: Attachment is NOT complete." << std::endl;
        return false;

    case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT_EXT:
        std::cout << "[ERROR] Framebuffer incomplete: No image is attached to FBO." << std::endl;
        return false;

    case GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS_EXT:
        std::cout << "[ERROR] Framebuffer incomplete: Attached images have different dimensions." << std::endl;
        return false;

    case GL_FRAMEBUFFER_INCOMPLETE_FORMATS_EXT:
        std::cout << "[ERROR] Framebuffer incomplete: Color attached images have different internal formats." << std::endl;
        return false;

    case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER_EXT:
        std::cout << "[ERROR] Framebuffer incomplete: Draw buffer." << std::endl;
        return false;

    case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER_EXT:
        std::cout << "[ERROR] Framebuffer incomplete: Read buffer." << std::endl;
        return false;

    case GL_FRAMEBUFFER_UNSUPPORTED_EXT:
        std::cout << "[ERROR] Unsupported by FBO implementation." << std::endl;
        return false;

    default:
        std::cout << "[ERROR] Unknow error." << std::endl;
        return false;
    }
}



void checkParticleGLErrors() 
{
   const GLenum errCode = glGetError();//check for errors
   if (errCode != GL_NO_ERROR)
   {
      std::cerr << "Particle Error: " << gluErrorString(errCode) << std::endl;
      //throw runtime_error((const char*)gluErrorString(errCode));
   }
}

void ParticleSystem::addEmitter(ParticleEmitter pe)
{
   m_pEmitters.push_back(pe);
}

void ParticleSystem::removeEmitter(ParticleEmitter pe)
{}

void ParticleSystem::addLocalForce(localForce lf)
{
   m_lforces.push_back(lf);
}

void ParticleSystem::removeLocalForce(localForce lf)// TODO
{}

void ParticleSystem::addGlobalForce(globalForce gf)
{
   m_gforces.push_back(gf);
}

void ParticleSystem::removeGlobalForce(globalForce gf)// TODO
{
}

void ParticleSystem::addCollision(collisionObject co)
{
   m_collision.push_back(co);
}

void ParticleSystem::removeCollision(collisionObject co)// TODO
{}

void ParticleSystem::animateEmitter(int i, CVector<float,3> direction, float step)
{
   m_pEmitters[i].m_position[0] -= direction[0] * step;
   m_pEmitters[i].m_position[1] -= direction[1] * step;
   m_pEmitters[i].m_position[2] -= direction[2] * step;
}


static CVector<float,3> normalize(CVector<float,3> a)
{
   float length = sqrt((a[0] * a[0]) + (a[1] * a[1]) + (a[2] * a[2]));
      a[0] = a[0] / length;
      a[1] = a[1] / length;
      a[2] = a[2] / length;

      return a;
}

static float dot(CVector<float,3> a, CVector<float,3> b)
{
     return (a[0] * b[0] + a[1] * b[1] + a[2] * b[2]);
}

void ParticleSystem::mirrorGlForceDirection(int i, CVector<float,3> direction)
{
   CVector<float,3> N(0., -1., 0.);
   CVector<float,3> R_m = direction-(2.0f * N * dot(direction, N));
   m_gforces[i].m_direction[0] = R_m[0];
   m_gforces[i].m_direction[1] = R_m[1];
   m_gforces[i].m_direction[2] = R_m[2];
}

///particle birth form all active emitters
void ParticleSystem::allocateParticles()
{
//////////////////
# ifdef DEBUG
   m_PosParticles.clear();
   m_PosParticles.resize(m_textureSize * m_textureSize * 4);
#endif
/////// read old textures back into data vectors
   glBindTexture(GL_TEXTURE_2D, m_textureId[m_flip * 2]);//last rendered (0,2)
   glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_FLOAT, &m_PosParticles[0]);
   glBindTexture(GL_TEXTURE_2D, 0);

   glBindTexture(GL_TEXTURE_2D, m_AttrtextureId[0]);
   glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_FLOAT, &m_AttrParticles[0]);
   glBindTexture(GL_TEXTURE_2D, 0);

/// emit 
   srand(time(0));
   for (int p = 0; p < m_pEmitters.size(); p++)
   {
      if (m_pEmitters[p].m_numOfNewParticlesMinMax[1]>0)
      {
         m_emitted = rand()% m_pEmitters[p].m_numOfNewParticlesMinMax[1] + m_pEmitters[p].m_numOfNewParticlesMinMax[0];
         float emp0 = m_pEmitters[p].m_position[0];
         float emp1 = m_pEmitters[p].m_position[1];
         float emp2 = m_pEmitters[p].m_position[2];

         float off0 = m_pEmitters[p].m_positionMaxOffset[0];
         float offDiff = (m_pEmitters[p].m_positionMaxOffset[1]-m_pEmitters[p].m_positionMaxOffset[0]);

         // TODO: use random table
         particle newPar;
         int j, iSize = 0;

         for(int i = 0; i < m_emitted ; i++)
         //for(int i = 0; i<1 ;i ++)
         {
            iSize = m_freeIndices.size();
      #ifdef DEBUG
            std::cout <<  "iSize:" << iSize<< std::endl;
      #endif
            if (iSize <= 0)
               return;
            j = m_freeIndices[iSize - 1] * 4;
      #ifdef DEBUG
            std::cout << j << " j: iSize " << iSize <<" : " <<
            m_PosParticles.size()<<" : " <<m_AttrParticles.size()
            <<" len: " << m_freeIndices.size()<< std::endl;
      #endif
            m_freeIndices.pop_back();
            newPar.m_position[0] = emp0 + ((rand()/rMax) * offDiff + off0);
            newPar.m_position[1] = emp1 + ((rand()/rMax) * offDiff + off0);
            newPar.m_position[2] = emp2 + ((rand()/rMax) * offDiff + off0);

            float v1 = m_pEmitters[p].m_position[0] + (rand() / rMax) *
               (m_pEmitters[p].m_positionMaxOffset[1]-m_pEmitters[p].m_positionMaxOffset[0]) + m_pEmitters[p].m_positionMaxOffset[0];
            float initialVelocityMinMax = (rand() / rMax) *
               (m_pEmitters[p].m_initialSpeedMinMax[1]-m_pEmitters[p].m_initialSpeedMinMax[0]) + m_pEmitters[p].m_initialSpeedMinMax[0];
            newPar.m_velocity = m_pEmitters[p].m_initialDirection * initialVelocityMinMax;

            newPar.m_size = m_pEmitters[p].m_initialSizeMinMax[0] + (rand() / m_pEmitters[p].m_initialSizeMinMax[1]) *
               (m_pEmitters[p].m_initialSizeMinMax[1]-m_pEmitters[p].m_initialSizeMinMax[0]) + m_pEmitters[p].m_initialSizeMinMax[0];
            newPar.m_timeOfDeath = m_currentTime + (rand()/m_pEmitters[p].m_lifeTimeMinMax[1]) *
               (m_pEmitters[p].m_lifeTimeMinMax[1]-m_pEmitters[p].m_lifeTimeMinMax[0]) + m_pEmitters[p].m_lifeTimeMinMax[0];
            newPar.m_mass = newPar.m_size;

            // fill texture data
            m_PosParticles[j] = newPar.m_position[0];
            m_PosParticles[j + 1] = newPar.m_position[1];
            m_PosParticles[j + 2] = newPar.m_position[2];
            m_PosParticles[j + 3] = 1.;

            m_AttrParticles[j] = newPar.m_mass;
            m_AttrParticles[j + 1] = (p + 1.)/10.;///emitter id
            m_AttrParticles[j + 2] = newPar.m_size;
            m_AttrParticles[j + 3] = newPar.m_timeOfDeath;
         }
      }
   }

# ifdef DEBUG
   m_AttrParticles[4] = .1;
   m_AttrParticles[5] = .2;
   m_AttrParticles[6] = .3;
   m_AttrParticles[7] = .4;
#endif

   // upload positions
   glBindTexture(GL_TEXTURE_2D, m_textureId[int((m_flip + 1) % 2 * 2)]);
   glTexSubImage2D(GL_TEXTURE_2D, 0, 0 , 0, m_textureSize, m_textureSize, GL_RGBA, GL_FLOAT, &m_PosParticles[0]);
   glBindTexture(GL_TEXTURE_2D,  0);

   // upload static attrs
   glBindTexture(GL_TEXTURE_2D, m_AttrtextureId[0]);
   glTexSubImage2D(GL_TEXTURE_2D, 0, 0 , 0, m_textureSize, m_textureSize, GL_RGBA, GL_FLOAT, &m_AttrParticles[0]);
   glBindTexture(GL_TEXTURE_2D,  0);

# ifdef DEBUG
   / *
   m_PosParticles.clear();//debug
   m_PosParticles.resize(256 * 256 * 4);
   glBindTexture(GL_TEXTURE_2D, m_textureId[(m_flip + 1) % 2 * 2]);//last rendered (0,2)
   glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_FLOAT, &m_PosParticles[0]);
   glBindTexture(GL_TEXTURE_2D, 0);
   std::cout << (m_flip + 1)%2 * 2<<" : " <<m_flip * 2<< " end allocateParticles:" << m_PosParticles[4] << " : "<< m_PosParticles[5] << " : " << m_PosParticles[6] << " : " << m_PosParticles[7] << std::endl;
    * /
#endif

# ifdef DEBUG
   m_AttrParticles.clear();//debug only
   m_AttrParticles.resize(m_textureSize * m_textureSize * 4);
   glBindTexture(GL_TEXTURE_2D, m_AttrtextureId[0]);
   glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_FLOAT, &m_AttrParticles[0]);
   glBindTexture(GL_TEXTURE_2D, 0);

   std::cout << m_AttrParticles[1]  << " end allocateParticles: "<< m_AttrParticles[5] << " : " << m_AttrParticles[9] << " : " << m_AttrParticles[13] << std::endl;

#endif
//   checkParticleGLErrors();

}

/////init
void ParticleSystem::setUp(int attributeType, float initTimeStep, GLuint simP, GLuint renderP, GLuint dbgP)
{
   simProg = simP;
   renderProg = renderP;
   debugShaderProg = dbgP;

   // create textures
    glGenTextures(4, m_textureId);
   for (int i = 0; i < 4; i++)
   {
      glBindTexture(GL_TEXTURE_2D, m_textureId[i]);
      glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
      glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
      glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
      glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
      glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
      glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE); // automatic mipmap generation included in OpenGL v1.4
      glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F_ARB, m_textureSize, m_textureSize, 0, GL_RGBA, GL_FLOAT, 0);
      //GL_TEXTURE_2D, 0,GL_RGB32F_ARB, 256, 256, 0, GL_RGBA , GL_FLOAT, 0
      glBindTexture(GL_TEXTURE_2D, 0);
   }

   //// static attrs: size, mass, death time
      glGenTextures(1, m_AttrtextureId);
      glBindTexture(GL_TEXTURE_2D, m_AttrtextureId[0]);
      glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
      glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
      glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
      glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
      glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
      glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE); // automatic mipmap generation included in OpenGL v1.4
      glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F_ARB, m_textureSize, m_textureSize,
        0, GL_RGBA, GL_FLOAT, 0);
      glBindTexture(GL_TEXTURE_2D, 0);

   // create a framebuffer object
    glGenFramebuffersEXT(2, m_fboId);
    glGenRenderbuffersEXT(4, m_rboId);

   for (int i = 0; i < 2; i++)
   {
        glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, m_fboId[i]);
        // create a renderbuffer object to store depth info
        // NOTE: A depth renderable image should be attached the FBO for depth test.
        // If we don't attach a depth renderable image to the FBO, then
        // the rendering output will be corrupted because of missing depth test.
        glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, m_rboId[i * 2]);
        glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT, GL_DEPTH_COMPONENT, m_textureSize, m_textureSize);
        glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, 0);

        // attach 2 textures to FBO color attachement point
        glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, m_textureId[i], 0);///???
      //glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, m_textureId[i + 1], 0);///???
        // attach 2 textures to FBO color attachement point
        glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_RENDERBUFFER_EXT, m_rboId[i * 2]);
       // glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_RENDERBUFFER_EXT, m_rboId[i + 2]);

      // check FBO status
      // printFramebufferInfo();
      bool status = checkFramebufferStatus();
      glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
   }
   // static attributes
   float data[tSize * tSize * 2]; // swich to member
   //texture coordinates for each pixel in the texture
   for (int i = 0; i < 256; i++)
   {
      for (int j = 0; j < 256; j++)
      {
         data[i * 2 + j * 512] = i / 256.0;
         data[i * 2 + j * 512 + 1] = j / 256.0;
      }
   }

   glGenBuffersARB(1, m_VerTextureBuffers);
   glBindBufferARB(GL_ARRAY_BUFFER_ARB, m_VerTextureBuffers[0]);
   glBufferDataARB(GL_ARRAY_BUFFER_ARB, m_textureSize * m_textureSize  * 2 * sizeof(float), data, GL_STATIC_DRAW_ARB);
   glBindBufferARB(GL_ARRAY_BUFFER_ARB, 0);
   glBindTexture(GL_TEXTURE_2D,0);

   reset();
   allocateParticles();
   //checkParticleGLErrors();
}


void ParticleSystem::displayParticles()
{
   if (m_drawAs == 0)
   {
      glPointSize(3.6);
      glEnable(GL_POINT_SPRITE);
   }
   else
   {
      glPointSize(1.5);
      glEnable(GL_POINT);
   }
#ifdef DEBUG
   std::cout << "displayParticles: " << m_flip * 2 <<std::endl;
#endif

   glActiveTexture(GL_TEXTURE0);// pos
   glBindTexture(GL_TEXTURE_2D, m_textureId[m_flip * 2]);// flip 0,2
   glActiveTexture(GL_TEXTURE2);//attrs
   glBindTexture(GL_TEXTURE_2D, m_AttrtextureId[0]);

   glPushMatrix();
   glUseProgram(renderProg);
   GLuint tHandle = glGetUniformLocation(renderProg, "Texture0");
   glUniform1i(tHandle, 0);
   GLuint tHandle1 = glGetUniformLocation(renderProg, "attrMap");
   glUniform1i(tHandle1, 2);

   glEnableClientState(GL_VERTEX_ARRAY);
   glBindBufferARB(GL_ARRAY_BUFFER_ARB, m_VerTextureBuffers[0]);
   glVertexPointer(2, GL_FLOAT, 0 , (char*) NULL);// texture coordinates
   glDrawArrays(GL_POINTS, 0, m_textureSize *m_textureSize);
   glPopMatrix();

   /////
   glDisableClientState(GL_VERTEX_ARRAY);
   glBindBufferARB(GL_ARRAY_BUFFER_ARB, 0);
   glUseProgram(0);
   glBindTexture(GL_TEXTURE_2D, 0);

   //checkParticleGLErrors();///////////stack
}

/ *
1. Process birth 
2. Update velocities
3. Update positions
5. Transfer texture data to vertex data
6. Render particle  * /
// position update
void ParticleSystem::updateParticles(float timeStep)
{
   m_currentTime += timeStep;
   m_flip = (m_flip + 1) % 2;
   allocateParticles();

#ifdef DEBUG
   std::cout << (m_flip + 1) % 2 * 2 << " updateParticles:" << m_flip * 2 << std::endl;
#endif

////// render to texture 
    // adjust viewport and projection matrix to texture dimension
    glPushMatrix();
    glViewport(0, 0, m_textureSize, m_textureSize);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    // procedure glOrtho(left, right, bottom, top, znear, zfar : double);
    glOrtho(-1.0f, 1.0f, -1.f, 1.f, -1.0f, 1.0f);
    glMatrixMode(GL_MODELVIEW);
 
    // with FBO  render directly to a texture   set the rendering destination to FBO
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, m_fboId[m_flip]);// flip 0,1
    // clear buffer
    glClearColor(1, 1, 1, 1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // draw 
    glUseProgram(simProg);
    glActiveTexture(GL_TEXTURE0);// pos
    glBindTexture(GL_TEXTURE_2D, m_textureId[(m_flip + 1) % 2 * 2]);///
    glActiveTexture(GL_TEXTURE1);//attrs
    glBindTexture(GL_TEXTURE_2D, m_AttrtextureId[0]);// no flip

    GLuint tHandle = glGetUniformLocation(simProg, "gForceDir");
    GLuint t3Handle = glGetUniformLocation(simProg, "gForceStrength");
    GLuint t1Handle = glGetUniformLocation(simProg, "vtimeElapsed");
    GLuint t2Handle = glGetUniformLocation(simProg, "groundPlane");

    if (m_lforces.size() > 0)
    {
      GLuint t4Handle = glGetUniformLocation(simProg, "localForcePosition");
      GLuint t5Handle = glGetUniformLocation(simProg, "localForceDir");
      GLuint t6Handle = glGetUniformLocation(simProg, "localForceStrength");
      GLuint t7Handle = glGetUniformLocation(simProg, "localForceMaxDistance");
      glUniform4fv(t5Handle, 1, &m_lforces[0].m_direction[0]);
      glUniform4fv(t4Handle, 1, &m_lforces[0].m_position[0]);
      glUniform1fv(t6Handle, 1, &m_lforces[0].m_strength);
      glUniform1fv(t7Handle, 1, &m_lforces[0].m_maxDistance);
   }
   float gStrength = m_gforces[0].m_strength;// * m_currentTime;
   //set values
   glUniform4fv(tHandle, 1, &m_gforces[0].m_direction[0]);
   glUniform1fv(t3Handle, 1, & gStrength);
   glUniform1fv(t1Handle, 1, &m_currentTime);
   glUniform1fv(t2Handle, 1, &m_groundPlane);

   //std::cout <<"m_gforces[0].m_direction[0]: "<< m_gforces[0].m_direction[0] <<" : "<< m_gforces[0].m_direction[1] <<" : "<<m_gforces[0].m_direction[1]<< std::endl;
    glPushMatrix();
      glLoadIdentity();
      glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
      glBegin(GL_QUADS);
         glTexCoord2f(0, 0);    glVertex3f(1., 0., 1.);
         glTexCoord2f(1., 0);   glVertex3f(1.,  0., -1.);
         glTexCoord2f(1., 1.);  glVertex3f(-1., 0., -1.);
         glTexCoord2f(0, 1.);   glVertex3f(-1., 0., 1.);
      glEnd();
    glPopMatrix();
    glPopMatrix();
   // copy the framebuffer pixels to a texture
    glBindTexture(GL_TEXTURE_2D,  m_textureId[(m_flip + 1) % 2 * 2]);
    glCopyTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 0, 0, m_textureSize, m_textureSize);

    /// back to normal window-system-provided framebuffer
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0); //unbind
    glBindTexture(GL_TEXTURE_2D, 0);
    glUseProgram(0);

    cleanUpParticles();
}

//CPU: Free particle index, re-add it to allocator
//GPU: Move m_particles to infinity 
void ParticleSystem::cleanUpParticles()
{
   float defaultTimeofDeath = 0.;
   float defaultVelocity = 0.;
   float defaultPosition = 1000.;

   int maxSize = m_textureSize * m_textureSize;

   for (int i = 0; i < m_PosParticles.size(); i += 4)
   {
      if (m_currentTime <= m_AttrParticles[i + 4])
      {
         if (m_freeIndices.size() >= maxSize)
            break;
      }
   }
}

///all particles are invisible 
void ParticleSystem::reset()
{
   // clear internal data
   m_PosParticles.clear();
   m_freeIndices.clear();
   m_AttrParticles.clear();
   m_particles.clear();

   m_PosParticles.resize(m_textureSize * m_textureSize * 4);
   m_AttrParticles.resize(m_textureSize * m_textureSize * 4);
   m_freeIndices.resize(m_textureSize * m_textureSize);

   float defaultTimeofDeath = 0.;
   float defaultSize = 1.;
   float defaultPosition = 1000.;
   float defaultMass = 0.;

   // init
   for (int i = m_textureSize * m_textureSize; i >= 0; i--)
      m_freeIndices[i] = i;

   for (int i = 0; i < m_textureSize * m_textureSize; i += 4)
   {
      m_PosParticles[i] = defaultPosition;
      m_PosParticles[i + 1] = defaultPosition;
      m_PosParticles[i + 2] = defaultPosition;
      m_PosParticles[i + 3] = 0.;// emitter id

      m_AttrParticles[i] = defaultMass;
      m_AttrParticles[i + 1] = 10.0;
      m_AttrParticles[i + 2] = defaultSize;
      m_AttrParticles[i + 3] = defaultTimeofDeath;
   }
   m_emitted = 0;
   m_active = 0;

   glBindTexture(GL_TEXTURE_2D, m_textureId[0]);
   glTexSubImage2D(GL_TEXTURE_2D, 0, 0 , 0, m_textureSize, m_textureSize, GL_RGBA, GL_FLOAT, &m_PosParticles[0]);
   glBindTexture(GL_TEXTURE_2D,  0);

   glBindTexture(GL_TEXTURE_2D, m_textureId[2]);
   glTexSubImage2D(GL_TEXTURE_2D, 0, 0 , 0, m_textureSize, m_textureSize, GL_RGBA, GL_FLOAT, &m_PosParticles[0]);
   glBindTexture(GL_TEXTURE_2D,  0);

}

///////////////debug
void ParticleSystem::drawEmitter(int i)
{
   glTranslatef(m_pEmitters[i].getPosition()[0], m_pEmitters[i].getPosition()[1],
          m_pEmitters[i].getPosition()[2]);
   glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
   glutWireCone(m_pEmitters[i].m_positionMaxOffset[1] * 2., 0.4, 5., 5.);
}


void ParticleSystem::displayGround(float Width, float Height)
{
   glBegin(GL_QUADS);
   glNormal3f(0., 0., 1.);
   glColor4f(1., 1., 1., 1.);
   glTexCoord2f(0., 0.); glVertex3f(Width, m_groundPlane, Height);
   glTexCoord2f(Width, 0.); glVertex3f(Width, m_groundPlane, -Height);
   glTexCoord2f(Width, Height); glVertex3f(-Width, m_groundPlane,-Height);
   glTexCoord2f(0., Height); glVertex3f(-Width, m_groundPlane, Height);
   glEnd();
}


void DrawQuad(float Width, float Height)//2d
{
   glBegin(GL_QUADS);
   glNormal3f(0., 0., 1.);
   glColor4f(1., 1., 1., 1.);
   glTexCoord2f(0, 0); glVertex2f(0., 0.);
   glTexCoord2f(Width, 0.); glVertex2f(Width, 0.);
   glTexCoord2f(Width, Height); glVertex2f(Width, Height);
   glTexCoord2f(0., Height); glVertex2f(0., Height);
   glEnd();
}


static float fvLightPosition[] = {-100.f, 100.f, 100.f};
static float fvEyePosition[] = {0.f, 0.f, 0.f};
static float fvDiffuse[] = {0.88f, 0.88f, 0.88f, 1.0f};
static float fvSpecular[] = {0.8f, 0.8f, 0.8f, 1.0f};
static float fvAmbient[] = {0.37f, 0.37f, 0.37f, 1.0f};
static float fSpecularPower[] = {25};
static float fvBaseColor[] = {0.7f, 0.7f, 0.0f, 1.0f};
static float fvSelectedColor[] = {0.f, 1.f, 0.0f, 1.0f};

// draw a cube with the dynamic texture
void ParticleSystem::displayDebug(int textID)// pass texture index
{
   glActiveTexture(GL_TEXTURE0);
   if (textID == 1)
      glBindTexture(GL_TEXTURE_2D, m_textureId[0]);
   else if (textID == 2)
      glBindTexture(GL_TEXTURE_2D, m_textureId[2]);
   else
      glBindTexture(GL_TEXTURE_2D, m_AttrtextureId[0]);

   glUseProgram(debugShaderProg);
   GLuint tHandle = glGetUniformLocation(debugShaderProg, "baseMap");
   glUniform1i(tHandle, 0);

   glPushMatrix();
   glTranslatef(-2., 1., 0.);
   glBegin(GL_QUADS);
        glColor4f(1., 1., 1., 1.);

        // face v0-v1-v2-v3
        glNormal3f(0., 0., 1.);
        glTexCoord2f(1, 1); glVertex3f(1, 1, 1);
        glTexCoord2f(0, 1); glVertex3f(-1, 1, 1);
        glTexCoord2f(0, 0); glVertex3f(-1, -1, 1);
        glTexCoord2f(1, 0); glVertex3f(1, -1, 1);

        // face v0-v3-v4-v5
        glNormal3f(1., 0., 0.);
        glTexCoord2f(0, 1); glVertex3f(1, 1, 1);
        glTexCoord2f(0, 0); glVertex3f(1, -1, 1);
        glTexCoord2f(1, 0); glVertex3f(1, -1, -1);
        glTexCoord2f(1, 1); glVertex3f(1, 1, -1);

        // face v0-v5-v6-v1
        glNormal3f(0., 1., 0.);
        glTexCoord2f(1, 0); glVertex3f(1, 1, 1);
        glTexCoord2f(1, 1); glVertex3f(1, 1, -1);
        glTexCoord2f(0, 1); glVertex3f(-1, 1, -1);
        glTexCoord2f(0, 0); glVertex3f(-1, 1, 1);

        // face  v1-v6-v7-v2
        glNormal3f(-1., 0., 0.);
        glTexCoord2f(1, 1); glVertex3f(-1, 1, 1);
        glTexCoord2f(0, 1); glVertex3f(-1, 1, -1);
        glTexCoord2f(0, 0); glVertex3f(-1, -1, -1);
        glTexCoord2f(1, 0); glVertex3f(-1, -1, 1);

        // face v7-v4-v3-v2
        glNormal3f(0., -1., 0.);
        glTexCoord2f(0, 0); glVertex3f(-1,-1, -1);
        glTexCoord2f(1, 0); glVertex3f(1, -1, -1);
        glTexCoord2f(1, 1); glVertex3f(1, -1, 1);
        glTexCoord2f(0, 1); glVertex3f(-1, -1, 1);

        // face v4-v7-v6-v5
        glNormal3f(0., 0., -1.);
        glTexCoord2f(0, 0); glVertex3f(1, -1, -1);
        glTexCoord2f(1, 0); glVertex3f(-1, -1, -1);
        glTexCoord2f(1, 1); glVertex3f(-1, 1, -1);
        glTexCoord2f(0, 1); glVertex3f(1, 1, -1);
    glEnd();
   glPopMatrix();

   glBindTexture(GL_TEXTURE_2D, 0);
   glUseProgram(0);
}
