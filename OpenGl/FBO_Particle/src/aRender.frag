#version 120
uniform sampler2D attrMap; //static attrs


void main()
{
   vec4 data0 = texture2D(attrMap, gl_TexCoord[0].st);
   if (data0.y>0.) // emitter id
   {
      if (data0.y <= .2)
         gl_FragColor = vec4(1.0, 0.0, 0.0, .5);
      else if (data0.y <= .3)
         gl_FragColor = vec4(.0, 0.0, 1.0, .5);
      else if (data0.y <= .4)
         gl_FragColor = vec4(.0, 1.0, .0, .5);
      else
         gl_FragColor = vec4(1.0, 1.0, .0, .5);
   }
   else
      gl_FragColor = vec4(data0.y, data0.y, data0.y, 1);
}