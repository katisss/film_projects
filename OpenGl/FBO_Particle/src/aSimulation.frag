#version 120
// updtae particle positions

uniform sampler2D Texture0; //pos
//uniform sampler2D Texture1; //velocity, not yet used
uniform sampler2D Texture2; //static attrs

uniform float vtimeElapsed;
uniform float groundPlane;
uniform vec4 gForceDir; 
uniform float gForceStrength;

// unused
uniform float dampGForce;
uniform float localForcePosition;
uniform float localForceDir;
uniform float localForceStrength;
uniform float localForceMaxDistance;

// only useful with velocity
vec4 bounce(vec4 inputV, vec4 normalV)
{
   //reflect (genType I, genType N)
   return reflect(inputV, normalV);// TODO dampening
}

vec4 constField(float strength) 
{
   return strength* gForceDir;
}
 
vec3 dampField(vec3 velocity, float strength) 
{
   return -velocity*strength;
}

 /*
vec3 noiseField(float strength) 
{
   vec3 result;
   seed = (seed * 1103515245u + 12345u); result.x = float(seed);
   seed = (seed * 1103515245u + 12345u); result.y = float(seed);
   seed = (seed * 1103515245u + 12345u); result.z = float(seed);
   return timeElapsed * strength * ((result / 4294967296.0) - vec3(0.5, 0.5, 0.5));
}
*/

void main()
{
   vec4 data0 = texture2D(Texture0, vec2(gl_TexCoord[0]));
   vec4 data1 = texture2D(Texture2, vec2(gl_TexCoord[0]));// static attrs
   if (data1.w >=  vtimeElapsed)// life time is up
   {
      gl_FragData[0] = vec4(1000., 1000., 1000., 1);
   }

   //data1.x // mass
   //data1.y // size

   vec4 newPos = data0 + constField(gForceStrength);

   //ground plane
   float yVal = newPos.y;
   if (yVal <=  groundPlane && groundPlane > -999.)
   {
      yVal = groundPlane;
   }

   //gl_FragData[0] = data0; //pass through
    gl_FragData[0] = vec4(newPos.x, yVal, newPos.z, data0.w);
    //gl_FragData[1] = data1;

}

