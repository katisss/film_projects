#ifndef Particle_H
#define Particle_H

#define GLEW_STATIC
#include <GL/glew.h>
#ifdef __MAC__
#   include <GLUT/glut.h>
#else
#  define FREEGLUT_STATIC
#  include <GL/glut.h>
#endif

#include <vector>
#include "Vector.h"
#include "force.h"
#include "collision.h"


/*
Basic scene set up:
Interactive camera controls
Menu

Basic particle system
Simulation
Emitters for creating and destroying particle
particle is assigned a set of attributes
Animation, m_particles are transformed and moved according to their dynamic attributes
collision detection between m_particles and specified 3D objects in the scene to make the m_particles interact with obstacles in the environment
calculations which take into account external forces (gravity, friction, wind, etc.)

Rendering
Add Lighting, shadows, Material and Texturing
Add a choice of primitives, optional geometry instancing and metaballs 
motion blurring based on the previous and current particle positions

Renderman Procedural
 Create a Renderman Procedural Dso using Aqsis (or 3delight) Renderer 
 A mechansism to provide the dso with camera and particle data 
 Maybe an option to increase level of detail in the render compared to the real time application

Speedup particle simulation and rendering by sending appropriate data to the gpu, I will investigarte this topic before starting so it might be part of the design

Optional features:
Let the user load custom cameras, hightfields and collision geometry 
Deformers that can work on particles
Sprites and metaballs
*/


// particle class
class particle
{
 public:
   particle(){}
   ~particle(){};

   int m_index;
   float m_timeOfDeath;
   float m_size;
   float m_mass;
   CVector<float,3> m_position;
   CVector<float,3> m_velocity;
   CVector<float,3> m_color;

   //   string DiffuseTextureName;
};

// 
class ParticleEmitter
{
 public:
   ParticleEmitter()
   {
    enable=1;
   }
   ~ParticleEmitter(){};
   void setPosition(CVector<float,3> newPosition)
      {    m_position=newPosition;    };
   CVector<float,3> getPosition()
      {    return m_position;    };

   bool enable;
   float m_StartTime;
   CVector<float,3> m_rotation;
   CVector<float,3> m_positionMaxOffset;
   CVector<float,3> m_initialDirection;
   CVector<float,2> m_initialSpeedMinMax;
   CVector<float,2> m_initialSizeMinMax;
   CVector<float,2> m_lifeTimeMinMax;
   CVector<int,2> m_numOfNewParticlesMinMax;
   CVector<float,3> m_position;
 private:

};

///
class ParticleSystem
{
public:
   ParticleSystem()
   {
      m_currentTime=0.;
      m_drawAs=0;
      m_timeStep=.001;
      m_textureSize=256;
      m_groundPlane=.0;
      m_flip=0;
      m_selectedEmitter=0;
   }
   ~ParticleSystem()
   {
      /*
      glDeleteTextures(4, m_textureId);
        glDeleteFramebuffersEXT(2, m_fboId);
        glDeleteRenderbuffersEXT(4, m_rboId);
      */
   };

   int   m_textureSize;
   int m_selectedEmitter;
   float m_groundPlane;// height
   float m_timeStep;
   int m_drawAs, m_emitted;
   GLuint m_locMovePosition, m_locMoveVelocity, m_active;
   CVector<float,3> m_birthColor;

   void addEmitter(ParticleEmitter pe);
   void removeEmitter(ParticleEmitter pe);
   void addLocalForce(localForce lf);
   void removeLocalForce(localForce lf);
   void addGlobalForce(globalForce gf);
   void removeGlobalForce(globalForce gf);
   void addCollision(collisionObject co);
   void removeCollision(collisionObject co);

   void updateParticles(float timeStep);

   // Gl Handles
   GLuint m_shaderid[3];
   GLuint m_VerTextureBuffers[1];
   std::vector <ParticleEmitter> m_pEmitters;
   std::vector <globalForce> m_gforces;
   std::vector <collisionObject> m_collision;
   std::vector <localForce> m_lforces;

   ///////// draw
   void drawEmitter(int i);
   void displayParticles();
   void displayGround(float Width, float Height);
   void displayDebug(int textID);
   void setUp(int attributeType, float initTimeStep, GLuint simP, GLuint renderP, GLuint dbgP);

   void animateEmitter(int i, CVector<float,3> direction, float step);
   void mirrorGlForceDirection(int i, CVector<float,3> direction);
   void reset();

private:
   GLuint m_fboId[2];                       // ID of FBO
   GLuint m_textureId[4];                   // ID of texture
   GLuint m_AttrtextureId[1];               // ID of texture
   GLuint m_rboId[4];
   int m_flip;
   float m_currentTime;

   std::vector< int> m_freeIndices;
   std::vector <particle> m_particles;
   std::vector <float> m_PosParticles, m_AttrParticles;

   void cleanUpParticles();
   void allocateParticles();
};

#endif

